// Server.cpp : 此文件包含 "main" 函数。程序执行将在此处开始并结束。
//

#include "pch.h"
#include <iostream>
#include "Server.h"
#include "VarList.h"

int main()
{
	Server::GetSingleton().InitServer(VarList());
	Server::GetSingleton().RunServer(0);
	Server::GetSingleton().ShutServer();
	return 0;
}