#pragma once

#include "../../Public/Utility/Singleton.h"

class VarList;

class Server : public Singleton<Server>
{
public:
	bool InitServer(VarList& args);
	bool RunServer(int timeInterval);
	bool ShutServer();
};