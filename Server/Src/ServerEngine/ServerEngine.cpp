#include "ServerEngine.h"
#include "VarList.h"
#include "Object/ObjectMgr.h"
#include "../RakNetSystem.h"

#ifndef GENERATE_OBJECT
#define GENERATE_OBJECT(PTR, OBJECT)\
{\
    PTR = dynamic_cast<OBJECT*>(m_pObjectMgr->AddObject(MY_NEW OBJECT()));\
    PTR->SetProperty("Name", #OBJECT);\
}
#endif

bool ServerEngine::InitEngine(VarList & args)
{
	m_pObjectMgr = MY_NEW ObjectMgr();

	GENERATE_OBJECT(m_pNetWorkSystem, RakNetSystem);

	return false;
}

bool ServerEngine::RunEngine(int timeInterval)
{
	m_pObjectMgr->Update(timeInterval);
	return false;
}

bool ServerEngine::ShutEngine()
{
	SAFE_DELETE(m_pObjectMgr);
	return false;
}
