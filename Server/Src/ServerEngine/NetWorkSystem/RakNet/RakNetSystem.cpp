#include "RakNetSystem.h"
#include "RakPeerInterface.h"
#include "RakSleep.h"
#include "Kbhit.h"
#include "Gets.h"
#include "MessageIdentifiers.h"
#include <iostream>

using namespace std;

static const std::string PORT_STRING = "6000";
static const int MAX_CONNECTIONS = 4;
static const std::string PASSWORD = "Rumpelstiltskin";

// Copied from Multiplayer.cpp
// If the first byte is ID_TIMESTAMP, then we want the 5th byte
// Otherwise we want the 1st byte
static unsigned char GetPacketIdentifier(RakNet::Packet *p)
{
	if (p == 0)
		return 255;

	if ((unsigned char)p->data[0] == ID_TIMESTAMP)
	{
		RakAssert(p->length > sizeof(RakNet::MessageID) + sizeof(RakNet::Time));
		return (unsigned char)p->data[sizeof(RakNet::MessageID) + sizeof(RakNet::Time)];
	}
	else
		return (unsigned char)p->data[0];
}

bool RakNetSystem::Init()
{
	m_peer = RakNet::RakPeerInterface::GetInstance();

	m_peer->SetIncomingPassword(PASSWORD.c_str(), (int)strlen(PASSWORD.c_str()));
	m_peer->SetTimeoutTime(30000, RakNet::UNASSIGNED_SYSTEM_ADDRESS);

	RakNet::SocketDescriptor socketDescriptors[2];
	socketDescriptors[0].port = atoi(PORT_STRING.c_str());
	socketDescriptors[0].socketFamily = AF_INET; // Test out IPV4
	socketDescriptors[1].port = atoi(PORT_STRING.c_str());
	socketDescriptors[1].socketFamily = AF_INET6; // Test out IPV6

	if (m_peer->Startup(MAX_CONNECTIONS, socketDescriptors, 1) != RakNet::RAKNET_STARTED)
	{
		return false;
	}

	m_peer->SetMaximumIncomingConnections(MAX_CONNECTIONS);
	m_peer->SetOccasionalPing(true);
	m_peer->SetUnreliableTimeout(1000);

	return false;
}

bool RakNetSystem::Run(int timeInterval)
{
	// This sleep keeps RakNet responsive
	RakSleep(30);

	// Get a packet from either the server or the client
	for (RakNet::Packet* p = m_peer->Receive(); p; m_peer->DeallocatePacket(p), p = m_peer->Receive())
	{
		// We got a packet, get the identifier with our handy function
		auto packetIdentifier = GetPacketIdentifier(p);

		// Check if this is a network message packet
		switch (packetIdentifier)
		{
		case ID_DISCONNECTION_NOTIFICATION:
			// Connection lost normally
			printf("ID_DISCONNECTION_NOTIFICATION from %s\n", p->systemAddress.ToString(true));
			break;


		case ID_NEW_INCOMING_CONNECTION:
			// Somebody connected.  We have their IP now
			printf("ID_NEW_INCOMING_CONNECTION from %s with GUID %s\n", p->systemAddress.ToString(true), p->guid.ToString());
			//clientID = p->systemAddress; // Record the player ID of the client

			printf("Remote internal IDs:\n");
			for (int index = 0; index < MAXIMUM_NUMBER_OF_INTERNAL_IDS; index++)
			{
				RakNet::SystemAddress internalId = m_peer->GetInternalID(p->systemAddress, index);
				if (internalId != RakNet::UNASSIGNED_SYSTEM_ADDRESS)
				{
					printf("%i. %s\n", index + 1, internalId.ToString(true));
				}
			}

			break;

		case ID_INCOMPATIBLE_PROTOCOL_VERSION:
			printf("ID_INCOMPATIBLE_PROTOCOL_VERSION\n");
			break;

		case ID_CONNECTED_PING:
		case ID_UNCONNECTED_PING:
			printf("Ping from %s\n", p->systemAddress.ToString(true));
			break;

		case ID_CONNECTION_LOST:
			// Couldn't deliver a reliable packet - i.e. the other system was abnormally
			// terminated
			printf("ID_CONNECTION_LOST from %s\n", p->systemAddress.ToString(true));;
			break;

		default:
			// The server knows the static data of all clients, so we can prefix the message
			// With the name data
			printf("%s\n", p->data);

			// Relay the message.  We prefix the name for other clients.  This demonstrates
			// That messages can be changed on the server before being broadcast
			// Sending is the same as before
			/*char message[2048] = {};
			sprintf(message, "%s", p->data);
			m_peer->Send(message, (const int)strlen(message) + 1, HIGH_PRIORITY, RELIABLE_ORDERED, 0, p->systemAddress, true);*/

			break;
		}

	}

	return false;
}

bool RakNetSystem::Shut()
{
	m_peer->Shutdown(300);
	RakNet::RakPeerInterface::DestroyInstance(m_peer);
	return false;
}
