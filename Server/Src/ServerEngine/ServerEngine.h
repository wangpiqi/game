#pragma once

#include "Singleton.h"
#include "MacroUtility.h"

class VarList;
class ObjectMgr;
class NetWorkSystem;

class DLL_EXPORT ServerEngine : public Singleton<ServerEngine>
{
public:
	bool InitEngine(VarList& args);
	bool RunEngine(int timeInterval);
	bool ShutEngine();

private:
	ObjectMgr* m_pObjectMgr{ nullptr };
	NetWorkSystem* m_pNetWorkSystem{};
};