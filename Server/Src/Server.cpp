#include "Server.h"
#include "VarList.h"
#include "ServerEngine/ServerEngine.h"
#include <windows.h>

static const int TIME_INTERVAL = 50;

bool Server::InitServer(VarList & args)
{
	ServerEngine::GetSingleton().InitEngine(args);
	return false;
}

bool Server::RunServer(int timeInterval)
{
	while (true)
	{
		ServerEngine::GetSingleton().RunEngine(TIME_INTERVAL);
		Sleep(TIME_INTERVAL);
	}
	return false;
}

bool Server::ShutServer()
{
	ServerEngine::GetSingleton().ShutEngine();
	return false;
}
