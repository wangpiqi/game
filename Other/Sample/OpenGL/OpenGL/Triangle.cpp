#include "stdafx.h"

GLfloat vVerts[] = {
    -0.5f, 0.0f, 0.0f,
    0.5f, 0.0f, 0.0f,
    0.0f, 0.5f, 0.0f };

GLShaderManager shaderManager;
GLBatch triangleBatch;

void ChangeSize(GLsizei w, GLsizei h)
{
    glViewport(0, 0, w, h);
}

void SetupRC()
{
    glClearColor(0.0f, 0.0f, 1.0f, 1.0f);

    shaderManager.InitializeStockShaders();

    triangleBatch.Begin(GL_TRIANGLES, 3);
    triangleBatch.CopyVertexData3f(vVerts);
    triangleBatch.End();
}

void RenderScene()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

    GLfloat vRed[] = { 1, 0, 0, 1 };
    shaderManager.UseStockShader(GLT_SHADER_IDENTITY, vRed);
    triangleBatch.Draw();

    glutSwapBuffers();
}

void SpecialKeys(int key, int x, int y)
{
    GLfloat stepSize = 0.025f;

    GLfloat& blockX = vVerts[0];
    GLfloat& blockY = vVerts[7];

    switch (key)
    {
    case GLUT_KEY_UP:
    {
        blockY += stepSize;
    }
    break;
    case GLUT_KEY_DOWN:
    {
        blockY -= stepSize;
    }
    break;
    case GLUT_KEY_LEFT:
    {
        blockX -= stepSize;
    }
    break;
    case GLUT_KEY_RIGHT:
    {
        blockX += stepSize;
    }
    break;
    }

    triangleBatch.CopyVertexData3f(vVerts);

    glutPostRedisplay();
}

int main(int argc, char* argv[])
{
    //gltSetWorkingDirectory(argv[0]);

    glutInit(&argc, argv);

    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH | GLUT_STENCIL);

    glutInitWindowSize(800, 600);

    glutCreateWindow("Triangle");

    glutReshapeFunc(ChangeSize);

    glutDisplayFunc(RenderScene);

    glutSpecialFunc(SpecialKeys);

    auto err = glewInit();
    if (err != GLEW_OK)
    {
        return 1;
    }

    SetupRC();

    glutMainLoop();

    return 0;
}