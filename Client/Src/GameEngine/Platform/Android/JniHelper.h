#pragma once

#include <jni.h>
#include <android/native_window.h>
#include <android/asset_manager.h>

class JniHelper
{
public:
    static JavaVM* getJavaVM();
    static void setJavaVM(JavaVM *javaVM);
    static bool getEnv(JNIEnv **env);
    static void setSurface(jobject surface);
    static ANativeWindow* getANativeWindow();
    static void setAssetManager(AAssetManager* mgr) { m_psAAssetManager = mgr; };
    static AAssetManager* getAssetManager() { return m_psAAssetManager; };
private:
    static JavaVM *m_psJavaVM;
    static jobject m_surface;
    static AAssetManager* m_psAAssetManager;
};