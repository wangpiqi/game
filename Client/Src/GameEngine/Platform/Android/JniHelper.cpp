#include <android/native_window_jni.h>
#include <Utility/MacroUtility.h>
#include "JniHelper.h"

JavaVM* JniHelper::m_psJavaVM = nullptr;
jobject JniHelper::m_surface = nullptr;
AAssetManager* JniHelper::m_psAAssetManager = nullptr;

jint JNI_OnLoad(JavaVM* vm, void* reserved)
{
    JNIEnv* env;
    if (vm->GetEnv(reinterpret_cast<void**>(&env), JNI_VERSION_1_6) != JNI_OK) {
        return -1;
    }

    JniHelper::setJavaVM(vm);

    return JNI_VERSION_1_6;
}

JavaVM* JniHelper::getJavaVM()
{
    return m_psJavaVM;
}

void JniHelper::setJavaVM(JavaVM *javaVM)
{
    m_psJavaVM = javaVM;
}

bool JniHelper::getEnv(JNIEnv **env)
{
    bool bRet = false;

    do
    {
        if (m_psJavaVM->GetEnv((void**)env, JNI_VERSION_1_6) != JNI_OK)
        {
            break;
        }

        if (m_psJavaVM->AttachCurrentThread(env, 0) < 0)
        {
            break;
        }

        bRet = true;
    } while (0);

    return bRet;
}

void JniHelper::setSurface(jobject surface)
{
    m_surface = surface;
}

ANativeWindow* JniHelper::getANativeWindow()
{
    JNIEnv* env = nullptr;
    JniHelper::getEnv(&env);

    jclass classID = env->FindClass("com/example/worldend/game/MainActivity");
    NULL_RETURN_VALUE(classID, nullptr);

    jmethodID methodID = env->GetStaticMethodID(classID, "getSurface", "()Ljava/lang/Object;");
    NULL_RETURN_VALUE(methodID, nullptr);

    jobject surface = env->CallStaticObjectMethod(classID, methodID);
    NULL_RETURN_VALUE(surface, nullptr);

    return ANativeWindow_fromSurface(env, surface);
}