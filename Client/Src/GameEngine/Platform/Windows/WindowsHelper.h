#pragma once

#include "Windows.h"
#include "MacroUtility.h"

class DLL_EXPORT WindowsHelper
{
public:
	static HWND getHWND() {
		return m_hwnd;
	};

	static void setHWND(HWND hwnd) { m_hwnd = hwnd; };

	static int getWidth() {
		return m_width;
	};
	static int getHeight() {
		return m_height;
	};

	static void setWidth(int width) { m_width = width; };
	static void setHeight(int height) { m_height = height; };

private:
	static HWND m_hwnd;
	static int m_width;
	static int m_height;
};