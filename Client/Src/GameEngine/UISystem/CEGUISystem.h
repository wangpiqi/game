#pragma once

#include <Object/Object.h>
#include "UISystem.h"

class CEGUISystem : public UISystem
{
public:
    virtual ~CEGUISystem() = default;

    virtual bool Init() override { return true; };
    virtual bool Run(int timeInterval) override { return true; };
    virtual bool Shut() override { return true; };
};