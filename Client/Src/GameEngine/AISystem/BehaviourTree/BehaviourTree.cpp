#include <GameEngine.h>
#include "BehaviourTree.h"
#include "types/internal/TestAgent.h"

#ifdef ANDROID
#include <android/asset_manager_jni.h>
#include <Platform/Android/JniHelper.h>
#endif

#if !BEHAVIAC_CCDEFINE_ANDROID
#include <Windows.h>
#include <tchar.h>
static void SetExePath()
{
#if BEHAVIAC_CCDEFINE_MSVC
	TCHAR szCurPath[_MAX_PATH];

	GetModuleFileName(NULL, szCurPath, _MAX_PATH);

	TCHAR* p = szCurPath;

	while (_tcschr(p, L'\\'))
	{
		p = _tcschr(p, L'\\');
		p++;
	}

	*p = L'\0';

	SetCurrentDirectory(szCurPath);
#endif
}
#endif

bool BehaviourTree::Init()
{
#if !BEHAVIAC_CCDEFINE_ANDROID
	SetExePath();
#endif
	InitBehavic();
	InitPlayer();
	return false;
}

bool BehaviourTree::Run(int timeInterval)
{
	if (m_status == behaviac::BT_RUNNING)
	{
		m_status = m_pAgent->btexec();
	}
	return false;
}

bool BehaviourTree::Shut()
{
	CleanupPlayer();
	CleanupBehaviac();
	return false;
}

bool BehaviourTree::InitBehavic()
{
#if !BEHAVIAC_CCDEFINE_ANDROID
    behaviac::Workspace::GetInstance()->SetFilePath("Resource/AI/exported");
#else
    behaviac::CLogger::SetLoggingLevel(BEHAVIAC_LOG_NONE);

	AAssetManager* mgr = JniHelper::getAssetManager();
	BEHAVIAC_ASSERT(mgr);
	behaviac::CFileManager::GetInstance()->SetAssetManager(mgr);

    behaviac::Workspace::GetInstance()->SetFilePath("assets:/Resource/AI/exported");
#endif

	behaviac::Workspace::GetInstance()->SetFileFormat(behaviac::Workspace::EFF_xml);

	return true;
}

bool BehaviourTree::InitPlayer()
{
	m_pAgent = behaviac::Agent::Create<TestAgent>();

	bool bRet = m_pAgent->btload("NewBehavior_1");
	if (bRet)
	{
		m_pAgent->btsetcurrent("NewBehavior_1");
	}

	return bRet;
}

void BehaviourTree::CleanupPlayer()
{
	behaviac::Agent::Destroy(m_pAgent);
}

void BehaviourTree::CleanupBehaviac()
{
	behaviac::Workspace::GetInstance()->Cleanup();
}
