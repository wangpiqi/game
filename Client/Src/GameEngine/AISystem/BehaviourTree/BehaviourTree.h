#pragma once

#include "../AISystem.h"
#include "types/behaviac_types.h"

class BehaviourTree : public AISystem
{
public:
	virtual bool Init() override;
	virtual bool Run(int timeInterval) override;
	virtual bool Shut() override;

private:
	bool InitBehavic();
	bool InitPlayer();

	void CleanupPlayer();
	void CleanupBehaviac();

	behaviac::Agent* m_pAgent{ nullptr };
	behaviac::EBTStatus m_status{ behaviac::BT_RUNNING };
};