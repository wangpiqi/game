#pragma once

#include "../../../../Public/Object/Object.h"

class AISystem : public Object
{
public:
	virtual ~AISystem() {};

	virtual bool Init() override { return true; };
	virtual bool Run(int timeInterval) override { return true; };
	virtual bool Shut() override { return true; };
};