#include "GameEngine.h"
#include <MacroUtility.h>
#include <RenderSystem/RenderSystem.h>
#include <RenderSystem/OpenGL/OpenGL.h>
#include <SoundSystem/SoundSystem.h>
#include <SoundSystem/Wwise/WwiseSoundEngine.h>
#include "FontSystem/FreeType.h"
#include "ScriptSystem/LuaScriptSystem.h"
#include "UISystem/CEGUISystem.h"
#ifdef _WIN32
#include <Windows.h>
#include "Platform/Windows/WindowsHelper.h"
#endif
#include "PlayerPrefs.h"
#include "AISystem/BehaviourTree/BehaviourTree.h"
#include "ScriptSystem/eluna/ElunaSystem.h"
#include "../RakNetSystem.h"

#ifndef GENERATE_OBJECT
#define GENERATE_OBJECT(PTR, OBJECT)\
{\
    PTR = dynamic_cast<OBJECT*>(m_pObjectMgr->AddObject(MY_NEW OBJECT()));\
    PTR->SetProperty("Name", #OBJECT);\
}
#endif

GameEngine::GameEngine()
{
    m_pObjectMgr = MY_NEW ObjectMgr();
}

GameEngine::~GameEngine()
{
    //SAFE_DELETE(m_pObjectMgr);
}

bool GameEngine::InitEngine(VarList& args)
{
#ifdef _WIN32
	HWND hWnd = args.get<HWND>(0);
	WindowsHelper::setHWND(hWnd);
	WindowsHelper::setWidth(args.get<int>(1));
	WindowsHelper::setHeight(args.get<int>(2));
#endif

	PlayerPrefs::SetInt("WindowWidth", args.get<int>(1));
	PlayerPrefs::SetInt("WindowHeight", args.get<int>(2));

    GENERATE_OBJECT(m_pRenderSystem, OpenGL);
    GENERATE_OBJECT(m_pSoundSystem, WwiseSoundEngine);
    //GENERATE_OBJECT(m_pFontSystem, FreeType);
    //GENERATE_OBJECT(m_pScriptSystem, ElunaSystem);
	GENERATE_OBJECT(m_pAISystem, BehaviourTree);
    GENERATE_OBJECT(m_pUISystem, CEGUISystem);
	GENERATE_OBJECT(m_pNetWorkSystem, RakNetSystem);

    return true;
}

bool GameEngine::RunEngine(int timeInterval)
{
    m_pObjectMgr->Update(timeInterval);
    return true;
}

bool GameEngine::ShutEngine()
{
    SAFE_DELETE(m_pObjectMgr);
    return true;
}

#ifdef ANDROID
bool GameEngine::InitAssetManager(jobject assetManager)
{
    return true;
}
#endif
