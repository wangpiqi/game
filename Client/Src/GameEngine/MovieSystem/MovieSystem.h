#pragma once

#include <Object/Object.h>

class MovieSystem : public Object
{
public:
    virtual ~MovieSystem() = default;

    virtual bool Init() override { return true; };
    virtual bool Run(int timeInterval) override { return true; };
    virtual bool Shut() override { return true; };
};