#pragma once

#include <RenderSystem/RenderSystem.h>
#include "Shader/ShaderManager.h"

class OpenGL final : public RenderSystem
{
public:
    OpenGL();
    ~OpenGL();

    virtual bool Init() override;
    virtual bool Run(int timeInterval) override;
    virtual bool Shut() override;

	void Draw();

private:
	/// \brief Create a window with the specified parameters
	/// \param esContext Application context
	/// \param title Name for title bar of window
	/// \param width Width in pixels of window to create
	/// \param height Height in pixels of window to create
	/// \param flags Bitfield for the window creation flags
	///         ES_WINDOW_RGB     - specifies that the color buffer should have R,G,B channels
	///         ES_WINDOW_ALPHA   - specifies that the color buffer should have alpha
	///         ES_WINDOW_DEPTH   - specifies that a depth buffer should be created
	///         ES_WINDOW_STENCIL - specifies that a stencil buffer should be created
	///         ES_WINDOW_MULTISAMPLE - specifies that a multi-sample buffer should be created
	/// \return GL_TRUE if window creation is succesful, GL_FALSE otherwise
	GLboolean ESUTIL_API esCreateWindow(const char *title, GLuint flags);

private:
	ESContext* m_esContext{};

	/// Window width
	GLint       m_width{};
	/// Window height
	GLint       m_height{};
};