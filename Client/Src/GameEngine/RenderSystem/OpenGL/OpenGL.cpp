#include <MacroUtility.h>
#include "OpenGL.h"
#ifdef _WIN32
#include "Platform/Windows/WindowsHelper.h"
#endif
#ifdef ANDROID
#include <android/native_window.h>
#include <Platform/Android/JniHelper.h>
#endif
#include "PlayerPrefs.h"
#include "Shader/ShaderManager.h"

#define CLEAR_COLOR 0.0f, 0.0f, 0.5f

// Draw a triangle using the shader pair created in Init()
//
static void DrawTriangle(ESContext *esContext)
{
	GLfloat vVertices[] = { 0.0f,  0.5f, 0.0f,
							 -0.5f, -0.5f, 0.0f,
							 0.5f, -0.5f, 0.0f
	};

	// Clear the color buffer
	glClear(GL_COLOR_BUFFER_BIT);

	// Use the program object
	glUseProgram(((UserData *)esContext->userData)->programObject);

	// Load the vertex data
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, vVertices);
	glEnableVertexAttribArray(0);

	glDrawArrays(GL_TRIANGLES, 0, 3);
}

// GetContextRenderableType()
//
//    Check whether EGL_KHR_create_context extension is supported.  If so,
//    return EGL_OPENGL_ES3_BIT_KHR instead of EGL_OPENGL_ES2_BIT
//
static EGLint GetContextRenderableType(EGLDisplay eglDisplay)
{
#ifdef EGL_KHR_create_context
	const char *extensions = eglQueryString(eglDisplay, EGL_EXTENSIONS);

	// check whether EGL_KHR_create_context is in the extension string
	if (extensions != NULL && strstr(extensions, "EGL_KHR_create_context"))
	{
		// extension is supported
		return EGL_OPENGL_ES3_BIT_KHR;
	}
#endif
	// extension is not supported
	return EGL_OPENGL_ES2_BIT;
}

OpenGL::OpenGL()
{
    m_esContext = MY_NEW ESContext();
    m_esContext->userData = MY_NEW UserData();
}

OpenGL::~OpenGL()
{
	SAFE_DELETE(m_esContext->userData);
    SAFE_DELETE(m_esContext);
}

bool OpenGL::Init()
{
#ifdef ANDROID
    m_esContext->eglNativeDisplay = EGL_DEFAULT_DISPLAY;
	m_esContext->eglNativeWindow = JniHelper::getANativeWindow();
	if (m_esContext->eglNativeWindow == nullptr)
        return false;
#endif

    esCreateWindow ( "Game", ES_WINDOW_RGB );

	GLuint programObject = ShaderManager::GetSingleton().CreateShader("Shader/Hello_Triangle.vs", "Shader/Hello_Triangle.ps");

	// Store the program object
	static_cast<UserData *>(m_esContext->userData)->programObject = programObject;

	glClearColor(CLEAR_COLOR, 0.0f);

    return true;
}

bool OpenGL::Run(int timeInterval)
{
	// Call update function if registered
	if (m_esContext->updateFunc != NULL)
	{
		m_esContext->updateFunc(m_esContext, timeInterval / 1000.0f);
	}

	Draw();

    return true;
}

bool OpenGL::Shut()
{
	glDeleteProgram(((UserData *)(m_esContext->userData))->programObject);
    return true;
}

void OpenGL::Draw()
{
	// Set the viewport
	glViewport(0, 0, m_width, m_height);

	DrawTriangle(m_esContext);

	eglSwapBuffers(m_esContext->eglDisplay, m_esContext->eglSurface);
}

GLboolean ESUTIL_API OpenGL::esCreateWindow(const char * title, GLuint flags)
{
#ifndef __APPLE__

	NULL_RETURN_VALUE(m_esContext, GL_FALSE);

#ifdef ANDROID
	// For Android, get the width/height from the window rather than what the
	// application requested.
	m_width = ANativeWindow_getWidth(m_esContext->eglNativeWindow);
	m_height = ANativeWindow_getHeight(m_esContext->eglNativeWindow);
#else
	m_width = PlayerPrefs::GetInt("WindowWidth");
	m_height = PlayerPrefs::GetInt("WindowHeight");
#endif

#ifdef _WIN32
	m_esContext->eglNativeWindow = WindowsHelper::getHWND();
#endif

	m_esContext->eglDisplay = eglGetDisplay(m_esContext->eglNativeDisplay);
	if (m_esContext->eglDisplay == EGL_NO_DISPLAY)
	{
		return GL_FALSE;
	}

	// Initialize EGL
    EGLint majorVersion{};
    EGLint minorVersion{};
	if (!eglInitialize(m_esContext->eglDisplay, &majorVersion, &minorVersion))
	{
		return GL_FALSE;
	}

	EGLint numConfigs = 0;
	EGLint attribList[] =
	{
	   EGL_RED_SIZE,       5,
	   EGL_GREEN_SIZE,     6,
	   EGL_BLUE_SIZE,      5,
	   EGL_ALPHA_SIZE,     (flags & ES_WINDOW_ALPHA) ? 8 : EGL_DONT_CARE,
	   EGL_DEPTH_SIZE,     (flags & ES_WINDOW_DEPTH) ? 8 : EGL_DONT_CARE,
	   EGL_STENCIL_SIZE,   (flags & ES_WINDOW_STENCIL) ? 8 : EGL_DONT_CARE,
	   EGL_SAMPLE_BUFFERS, (flags & ES_WINDOW_MULTISAMPLE) ? 1 : 0,
	   // if EGL_KHR_create_context extension is supported, then we will use
	   // EGL_OPENGL_ES3_BIT_KHR instead of EGL_OPENGL_ES2_BIT in the attribute list
	   EGL_RENDERABLE_TYPE, GetContextRenderableType(m_esContext->eglDisplay),
	   EGL_NONE
	};

	// Choose config
	EGLConfig config;
	if (!eglChooseConfig(m_esContext->eglDisplay, attribList, &config, 1, &numConfigs))
	{
		return GL_FALSE;
	}

	if (numConfigs < 1)
	{
		return GL_FALSE;
	}

#ifdef ANDROID
	// For Android, need to get the EGL_NATIVE_VISUAL_ID and set it using ANativeWindow_setBuffersGeometry
	{
		EGLint format = 0;
		eglGetConfigAttrib(m_esContext->eglDisplay, config, EGL_NATIVE_VISUAL_ID, &format);
		ANativeWindow_setBuffersGeometry(m_esContext->eglNativeWindow, 0, 0, format);
	}
#endif // ANDROID

	// Create a surface
	m_esContext->eglSurface = eglCreateWindowSurface(m_esContext->eglDisplay, config,
		m_esContext->eglNativeWindow, NULL);

	if (m_esContext->eglSurface == EGL_NO_SURFACE)
	{
		return GL_FALSE;
	}

	// Create a GL context
    EGLint contextAttribs[] = { EGL_CONTEXT_CLIENT_VERSION, 3, EGL_NONE };
	m_esContext->eglContext = eglCreateContext(m_esContext->eglDisplay, config,
		EGL_NO_CONTEXT, contextAttribs);

	if (m_esContext->eglContext == EGL_NO_CONTEXT)
	{
		return GL_FALSE;
	}

	// Make the context current
	if (!eglMakeCurrent(m_esContext->eglDisplay, m_esContext->eglSurface,
		m_esContext->eglSurface, m_esContext->eglContext))
	{
		return GL_FALSE;
	}

#endif // #ifndef __APPLE__

	return GL_TRUE;
}