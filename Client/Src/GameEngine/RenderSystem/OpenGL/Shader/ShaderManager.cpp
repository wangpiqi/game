#include "ShaderManager.h"
#include "Shader.h"
#include "MacroUtility.h"
#include "FileUtility.h"
#include <string>

GLuint ShaderManager::CreateShader(const char* vsPath, const char* psPath)
{
	// Load the vertex/fragment shaders
	GLuint vertexShader = CreateVertexShader(vsPath);
	GLuint fragmentShader = CreateFragmentShader(psPath);

	// Create the program object
	GLuint programObject = glCreateProgram();
	if (programObject == 0)
	{
		return 0;
	}

	glAttachShader(programObject, vertexShader);
	glAttachShader(programObject, fragmentShader);

	// Link the program
	glLinkProgram(programObject);

	// Check the link status
	GLint linked;
	glGetProgramiv(programObject, GL_LINK_STATUS, &linked);

	if (!linked)
	{
		GLint infoLen = 0;

		glGetProgramiv(programObject, GL_INFO_LOG_LENGTH, &infoLen);

		if (infoLen > 1)
		{
			char *infoLog = static_cast<char*>(malloc(sizeof(char) * infoLen));

			glGetProgramInfoLog(programObject, infoLen, NULL, infoLog);
			//esLogMessage("Error linking program:\n%s\n", infoLog);

			free(infoLog);
		}

		glDeleteProgram(programObject);
		return FALSE;
	}

	return programObject;
}

GLuint ShaderManager::CreateVertexShader(const char * path)
{
	return CreateShaderInner(GL_VERTEX_SHADER, FileUtility::ReadFile(path).c_str());
}

GLuint ShaderManager::CreateFragmentShader(const char * path)
{
	return CreateShaderInner(GL_FRAGMENT_SHADER, FileUtility::ReadFile(path).c_str());
}

GLuint ShaderManager::CreateShaderInner(GLenum type, const char * shaderSrc)
{
	Shader* pShader = MY_NEW Shader();
	pShader->Load(type, shaderSrc);
	return pShader->getShader();
}
