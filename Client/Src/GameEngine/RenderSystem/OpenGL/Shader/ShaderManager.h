#pragma once

#include "Singleton.h"
#include <string>
#include "RenderSystem/OpenGL/opengles3/Common/Include/esUtil.h"

typedef struct
{
	// Handle to a program object
	GLuint programObject;

} UserData;

class ShaderManager : public Singleton<ShaderManager>
{
public:
	GLuint CreateShader(const char* vsPath, const char* psPath);

private:
	GLuint CreateVertexShader(const char* path);
	GLuint CreateFragmentShader(const char* path);

	// Create a shader object, load the shader source, and
	// compile the shader.
	//
	GLuint CreateShaderInner(GLenum type, const char *shaderSrc);
};