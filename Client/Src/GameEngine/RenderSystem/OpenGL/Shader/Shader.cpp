#include "Shader.h"

bool Shader::Load(GLenum type, const char * shaderSrc)
{
	// Create the shader object
	m_shader = glCreateShader(type);

	if (m_shader == 0)
	{
		return false;
	}

	// Load the shader source
	glShaderSource(m_shader, 1, &shaderSrc, NULL);

	// Compile the shader
	glCompileShader(m_shader);

	// Check the compile status
	glGetShaderiv(m_shader, GL_COMPILE_STATUS, &m_compiled);

	if (!m_compiled)
	{
		GLint infoLen = 0;

		glGetShaderiv(m_shader, GL_INFO_LOG_LENGTH, &infoLen);

		if (infoLen > 1)
		{
			char *infoLog = (char *)malloc(sizeof(char) * infoLen);

			glGetShaderInfoLog(m_shader, infoLen, NULL, infoLog);
			//esLogMessage("Error compiling shader:\n%s\n", infoLog);

			free(infoLog);
		}

		glDeleteShader(m_shader);
		return false;
	}

	return true;
}
