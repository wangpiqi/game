#pragma once

#include "RenderSystem/OpenGL/opengles3/Common/Include/esUtil.h"

class Shader
{
public:
	bool Load(GLenum type, const char * shaderSrc);

	GLuint getShader() {
		return  m_shader;
	};

private:
	GLuint m_shader{};
	GLint m_compiled{};
};