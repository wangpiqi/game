#pragma once

#include <Object/Object.h>

class RenderSystem : public Object
{
public:
    virtual ~RenderSystem() {};

    virtual bool Init() override { return true; };
    virtual bool Run(int timeInterval) override { return true; };
    virtual bool Shut() override { return true; };
};