#pragma once

#include "../../../../Public/Object/Object.h"

class NetWorkSystem : public Object
{
public:
	virtual ~NetWorkSystem() {};

	virtual bool Init() override { return true; };
	virtual bool Run(int timeInterval) override { return true; };
	virtual bool Shut() override { return true; };
};