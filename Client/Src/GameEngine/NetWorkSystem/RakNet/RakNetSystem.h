#pragma once

#include "../NetWorkSystem.h"

namespace RakNet
{
	class RakPeerInterface;
}

class RakNetSystem : public NetWorkSystem
{
public:
	virtual ~RakNetSystem() {};

	virtual bool Init() override;
	virtual bool Run(int timeInterval) override;
	virtual bool Shut() override;

private:
	RakNet::RakPeerInterface *m_peer{};
};