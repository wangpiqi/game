#pragma once

#ifndef _GAMEENGINE_H_
#define _GAMEENGINE_H_

#include <Singleton.h>
#include <Object/ObjectMgr.h>
#include "VarList.h"
#include "MacroUtility.h"

#ifdef ANDROID
#include <jni.h>
#endif

class ObjectMgr;
class RenderSystem;
class AISystem;
class SoundSystem;
class FontSystem;
class ScriptSystem;
class UISystem;
class NetWorkSystem;

class DLL_EXPORT GameEngine : public Singleton<GameEngine>
{
public:
    GameEngine();
    ~GameEngine();

    bool InitEngine(VarList& args);
    bool RunEngine(int timeInterval);
    bool ShutEngine();

    SoundSystem* GetSoundSystem() { return m_pSoundSystem; };
    FontSystem* GetFontSystem() { return m_pFontSystem; };

#ifdef ANDROID
	bool InitAssetManager(jobject assetManager);
#endif

private:
    ObjectMgr* m_pObjectMgr {nullptr};
    RenderSystem* m_pRenderSystem {nullptr};
	AISystem* m_pAISystem{};
    SoundSystem* m_pSoundSystem {nullptr};
    FontSystem* m_pFontSystem{};
    ScriptSystem* m_pScriptSystem{};
    UISystem* m_pUISystem{};
	NetWorkSystem* m_pNetWorkSystem{};
};

#endif