#pragma once

#include "ScriptSystem.h"

struct lua_State;

class LuaScriptSystem final : public ScriptSystem
{
public:
    virtual ~LuaScriptSystem() = default;

    virtual bool Init() override;
    virtual bool Run(int timeInterval) override;
    virtual bool Shut() override;

	const char* luaGetGlobalStr(const char* name);
	double luaGetGlobalNumber(const char* name);

private:
    lua_State * m_pLuaState{};
};