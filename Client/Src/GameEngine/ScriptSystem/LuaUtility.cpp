#include "LuaUtility.h"
#include <iostream>

void LuaUtility::StackDump(lua_State* pLuaState)
{
    std::cout << "begin dump lua stack" << std::endl;

    for (int i = 1; i <= lua_gettop(pLuaState); ++i)
    {
        int t = lua_type(pLuaState, i);
        switch (t)
        {
        case LUA_TSTRING:
        {
            printf("'%s' ", lua_tostring(pLuaState, i));
        }
        break;
        case LUA_TBOOLEAN:
        {
            printf(lua_toboolean(pLuaState, i) ? "true " : "false ");
        }
        break;
        case LUA_TNUMBER:
        {
            printf("%g ", lua_tonumber(pLuaState, i));
        }
        break;
        default:
        {
            printf("%s ", lua_typename(pLuaState, t));
        }
        break;
        }
        std::cout << std::endl;
    }

    std::cout << "end dump lua stack" << std::endl;
}
