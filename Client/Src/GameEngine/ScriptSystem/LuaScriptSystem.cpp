#include "LuaScriptSystem.h"
#include "lua-5.3.5/src/lua.hpp"
#include "../../../../Public/Utility/MacroUtility.h"
#include <assert.h>
#include <iostream>
#include "LuaUtility.h"

int Add2Number(lua_State* L)
{
    int a = lua_tonumber(L, -1);
    int b = lua_tonumber(L, -2);
    int sum = a + b;
    lua_pushnumber(L, sum);
    return 1;
}

bool LuaScriptSystem::Init()
{
    m_pLuaState = luaL_newstate();
    NULL_RETURN_VALUE(m_pLuaState, false);

    luaL_openlibs(m_pLuaState);

    {
        int ret = luaL_dofile(m_pLuaState, "Resource/Script/lua/test.lua");
        assert(ret == 0);

        std::cout << "**********************************************" << std::endl;

        std::cout << luaGetGlobalStr("name") << std::endl;

        std::cout << luaGetGlobalNumber("version") << std::endl;

        lua_getglobal(m_pLuaState, "me");

        lua_getfield(m_pLuaState, -1, "name");
        std::cout << lua_tolstring(m_pLuaState, -1, 0) << std::endl;
        lua_getfield(m_pLuaState, -2, "gender");
        std::cout << lua_tolstring(m_pLuaState, -1, 0) << std::endl;

        lua_pushstring(m_pLuaState, "007");
        lua_setfield(m_pLuaState, 3, "name");
        std::cout << lua_tolstring(m_pLuaState, -2, 0) << std::endl;

        lua_register(m_pLuaState, "Add2Number", Add2Number);

        lua_getglobal(m_pLuaState, "add");
        lua_pushnumber(m_pLuaState, 15);
        lua_pushnumber(m_pLuaState, 5);
        lua_pcall(m_pLuaState, 2, 1, NULL);//2-参数格式，1-返回值个数，调用函数，函数执行完，会将返回值压入栈中

        LuaUtility::StackDump(m_pLuaState);

        std::cout << "**********************************************" << std::endl;
    }

    return false;
}

bool LuaScriptSystem::Run(int timeInterval)
{
    return false;
}

bool LuaScriptSystem::Shut()
{
    lua_close(m_pLuaState);
    return false;
}

const char * LuaScriptSystem::luaGetGlobalStr(const char * name)
{
	lua_getglobal(m_pLuaState, name);
	return lua_tolstring(m_pLuaState, -1, 0);
}

double LuaScriptSystem::luaGetGlobalNumber(const char * name)
{
	lua_getglobal(m_pLuaState, name);
	return lua_tonumber(m_pLuaState, -1);
}
