#pragma once

#include <Singleton.h>
#include <Object/Object.h>

class ScriptSystem : public Object
{
public:
    virtual ~ScriptSystem() {};

    virtual bool Init() override;
    virtual bool Run(int timeInterval) override;
    virtual bool Shut() override;
};