#pragma once

#include "lua-5.3.5/src/lua.hpp"

class LuaUtility
{
public:
    static void StackDump(lua_State* pLuaState);
};