#include <Utility/LogUtility.h>
#include "ElunaSystem.h"
#include "ELuna.h"

bool ElunaSystem::Init()
{
	m_pLuaState = ELuna::openLua();

	ELuna::doFile(m_pLuaState, "Resource/Script/lua/test.lua");

	ELuna::LuaFunction<int> luaSum(m_pLuaState, "luaSum");

	LOGE("luaSum: %d\n", luaSum(1, 2));

	return false;
}

bool ElunaSystem::Run(int timeInterval)
{
	return false;
}

bool ElunaSystem::Shut()
{
	ELuna::closeLua(m_pLuaState);
	return false;
}
