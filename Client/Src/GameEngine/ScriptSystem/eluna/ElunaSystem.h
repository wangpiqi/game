#pragma once

#include "../ScriptSystem.h"

struct lua_State;

class ElunaSystem final : public ScriptSystem
{
public:
	virtual ~ElunaSystem() = default;

	virtual bool Init() override;
	virtual bool Run(int timeInterval) override;
	virtual bool Shut() override;

private:
	lua_State * m_pLuaState{};
};