#pragma once

#include <Singleton.h>
#include <Object/Object.h>

class SoundSystem : public Object
{
public:
    virtual ~SoundSystem() {};

    virtual bool Init() override;
    virtual bool Run(int timeInterval) override;
    virtual bool Shut() override;

    virtual bool Play(const char* name) { return true; };
    virtual bool Stop(const char* name) { return true; };
};