#include <assert.h>
#include "WwiseSoundBankMgr.h"
#include <MacroUtility.h>
#include <vector>
#include <string>
#include <LogUtility.h>
#include "StringUtility.h"
#include "FileUtility.h"

std::vector<std::string> vecBank{ "init.bnk",
//                                  "InteractiveMusic.bnk",
                                  "music_desert.bnk" };

WwiseSoundBankMgr::WwiseSoundBankMgr()
{
    m_plowLevelIO = MY_NEW CAkFilePackageLowLevelIOBlocking();
}

WwiseSoundBankMgr::~WwiseSoundBankMgr()
{
    SAFE_DELETE(m_plowLevelIO);
}

bool WwiseSoundBankMgr::LoadResource()
{
#ifdef AK_WIN
    m_plowLevelIO->SetBasePath(StringUtility::StringToWStr(FileUtility::GetResourcePath() + "Sound/windows").c_str());
#endif
#ifdef AK_ANDROID
    if (m_plowLevelIO->SetBasePath((FileUtility::GetResourcePath() + "Sound/android").c_str()) != AK_Success)
    {
        LOGE("lowLevelIO SetBasePath %s Failed!", (FileUtility::GetResourcePath() + "Sound/android").c_str());
        return false;
    }
#endif

    ::AK::StreamMgr::SetCurrentLanguage(AKTEXT("English(US)"));

    if (!LoadBank())
        return false;

    return true;
}

bool WwiseSoundBankMgr::LoadBank()
{
    for (auto bankName : vecBank)
    {
        LoadBank(bankName.c_str());
    }
    return true;
}

bool WwiseSoundBankMgr::LoadBank(const char* szBankName)
{
    AkBankID bankID; // 未使用。这些 SoundBank 可以通过它们的文件名来卸载。
    AKRESULT eResult = AK::SoundEngine::LoadBank( szBankName, AK_DEFAULT_POOL_ID, bankID );
//    assert( eResult == AK_Success );
    if (eResult != AK_Success)
    {
        LOGE("WwiseSoundBankMgr::LoadBank %s Failed!", szBankName);
    }
    return eResult == AK_Success;
}

bool WwiseSoundBankMgr::InitLowLevelIO()
{
    //
    // 创建采用阻塞型 Low-Level I/O 握手机制的 Streaming Manager。
    // 注意，可以使用您自己的 Low-Level I/O 模块覆盖默认的 Low-Level I/O 模块。请参阅
    // SDK 文档了解更多信息。
    //
    AkDeviceSettings deviceSettings;
    AK::StreamMgr::GetDefaultDeviceSettings(deviceSettings);

    // 在此自定义流播放设备设置。

    // CAkFilePackageLowLevelIOBlocking::Init() 在 Stream Manager 中
    // 创建流播放设备，并将其自身注册为 File Location Resolver 。
    if (m_plowLevelIO->Init(deviceSettings) != AK_Success)
    {
        assert(!"Could not create the streaming device and Low-Level I/O system");
        return false;
    }

    return true;
}

#ifdef AK_ANDROID
bool WwiseSoundBankMgr::InitAndroidIO(AkPlatformInitSettings& platformInitSettings)
{
    if (m_plowLevelIO->InitAndroidIO(platformInitSettings.pJavaVM, platformInitSettings.jNativeActivity) != AK_Success)
    {
        LOGE("lowLevelIO InitAndroidIO Failed!");
        return false;
    }

    m_plowLevelIO->AddBasePath("/sdcard");

    return true;
}
#endif

bool WwiseSoundBankMgr::TermLowLevelIO()
{
    // CAkFilePackageLowLevelIOBlocking::Term() 销毁 Stream Manager 中
    // 与其相关的流播放设备，并注销自己的 File Location Resolver 身份。
    m_plowLevelIO->Term();
    return true;
}