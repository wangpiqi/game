#pragma once

#include <AK/SoundEngine/Common/AkSoundEngine.h>
#ifdef AK_WIN
#include <Win32/AkFilePackageLowLevelIOBlocking.h>                    //  Low-Level I/O 实现示例
#endif
#ifdef AK_ANDROID
#include <Android/AkFilePackageLowLevelIOBlocking.h>                    //  Low-Level I/O 实现示例
#include <Platform/Android/JniHelper.h>
#endif

class CAkFilePackageLowLevelIOBlocking;

class WwiseSoundBankMgr
{
public:
    WwiseSoundBankMgr();
    ~WwiseSoundBankMgr();

    bool LoadResource();
    bool LoadBank(const char* szBankName);

    bool InitLowLevelIO();
#ifdef AK_ANDROID
    bool InitAndroidIO(AkPlatformInitSettings& platformInitSettings);
#endif
    bool TermLowLevelIO();

private:
    bool LoadBank();

    // 我们使用作为 SDK 代码示例一部分的
    // 默认 Low-Level I/O 实现，文件包扩展名为
    CAkFilePackageLowLevelIOBlocking* m_plowLevelIO{nullptr};
};