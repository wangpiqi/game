#include "WwiseSoundEngine.h"
#include <malloc.h>
#include <stdio.h>
#include <AK/SoundEngine/Common/AkMemoryMgr.h>                  // Memory Manager
#include <AK/SoundEngine/Common/AkModule.h>                     // 默认内存和流管理器
#include <AK/SoundEngine/Common/IAkStreamMgr.h>                 // Streaming Manager
#include <AK/Tools/Common/AkPlatformFuncs.h>                    // 线程定义
#include <AK/SoundEngine/Common/AkSoundEngine.h>                // 声音引擎
#include <AK/MusicEngine/Common/AkMusicEngine.h>                // 音乐引擎
// 为允许 Wwise 和游戏之间进行通信，包含以下代码——发布版本不需要
#ifndef AK_OPTIMIZED
#include <AK/Comm/AkCommunication.h>
#endif // AK_OPTIMIZED
#ifdef AK_WIN
#include <Win32/AkFilePackageLowLevelIOBlocking.h>                    //  Low-Level I/O 实现示例
#endif
#ifdef AK_ANDROID
#include <Android/AkFilePackageLowLevelIOBlocking.h>                    //  Low-Level I/O 实现示例
#include <Platform/Android/JniHelper.h>
#endif
#ifdef AK_APPLE
#include <POSIX/AkFilePackageLowLevelIOBlocking.h>                    //  Low-Level I/O 实现示例
#endif
#include <AK/SpatialAudio/Common/AkSpatialAudio.h>
#include <AK/Plugin/AllPluginsFactories.h>
#include <assert.h>
#include <MacroUtility.h>
#include <LogUtility.h>

/////////////////////////////////////////////////////////////////////////////////
// 自定义分配/释放函数。它们在 AkMemoryMgr.h 中被声明为“extern”，
// 必须由游戏程序员定义。
/////////////////////////////////////////////////////////////////////////////////
namespace AK
{
    void * AllocHook( size_t in_size )
    {
        return malloc( in_size );
    }
    void FreeHook( void * in_ptr )
    {
        free( in_ptr );
    }
#ifdef WIN32
    // 注：Stream Manager 默认实现的 I/O 池可以使用 VirtualAllocHook()
    // 来支持“真正的”非缓冲 I/O（使用 FILE_FLAG_NO_BUFFERING
    // - 请参阅 Windows SDK 文档了解更多详情）。这不是强制要求；
    // 您可以使用一个简单的 malloc() 来实现它。
    void * VirtualAllocHook(
        void * in_pMemAddress,
        size_t in_size,
        DWORD in_dwAllocationType,
        DWORD in_dwProtect
        )
    {
        return VirtualAlloc( in_pMemAddress, in_size, in_dwAllocationType, in_dwProtect );
    }
    void VirtualFreeHook(
        void * in_pMemAddress,
        size_t in_size,
        DWORD in_dwFreeType
        )
    {
        VirtualFree( in_pMemAddress, in_size, in_dwFreeType );
    }
#endif
}

WwiseSoundEngine::WwiseSoundEngine()
{
    m_upWwiseSoundBankMgr = std::make_unique<WwiseSoundBankMgr>();
}

WwiseSoundEngine::~WwiseSoundEngine()
{
    //
}

bool WwiseSoundEngine::Init()
{
    if (!InitWwiseSoundEngine())
    {
        LOGE("InitWwiseSoundEngine Failed!");
        return false;
    }

    if (!m_upWwiseSoundBankMgr->LoadResource())
    {
        return false;
    }

    return true;
}

bool WwiseSoundEngine::Run(int timeInterval)
{
    // 处理音频包请求、事件、位置、RTPC 等
    AK::SoundEngine::RenderAudio();
    return true;
}

bool WwiseSoundEngine::Shut()
{
#ifndef AK_OPTIMIZED
    //
    // 终止通信服务
    //
    AK::Comm::Term();
#endif // AK_OPTIMIZED

    //
    // 终止音乐引擎
    //

    AK::MusicEngine::Term();

    //
    // 终止声音引擎
    //

    AK::SoundEngine::Term();

    // 终止流播放设备和流播放管理器

    m_upWwiseSoundBankMgr->TermLowLevelIO();

    if (AK::IAkStreamMgr::Get())
        AK::IAkStreamMgr::Get()->Destroy();

    // 终止 Memory Manager
    AK::MemoryMgr::Term();

    return true;
}

bool WwiseSoundEngine::Play(const char* name)
{
    PostEvent(name);
    return true;
}

bool WwiseSoundEngine::Stop(const char* name)
{
    return true;
}

AkPlayingID WwiseSoundEngine::PostEvent(const char* in_pszEventName, AkGameObjectID in_gameObjectID)
{
    auto playingID = AK::SoundEngine::PostEvent(in_pszEventName, in_gameObjectID);
//    assert(playingID != AK_INVALID_PLAYING_ID);
    if (playingID == AK_INVALID_PLAYING_ID)
    {
        LOGE("WwiseSoundEngine::PostEvent %s Failed!", in_pszEventName);
    }
    return playingID;
}

bool WwiseSoundEngine::InitWwiseSoundEngine()
{
    if (!InitMemoryManager())
        return false;

    if (!InitStreamMgr())
        return false;

    if (!InitSoundEngine())
        return false;

    if (!InitMusicEngine())
        return false;

    if (!InitSpatialAudio())
        return false;

    if (!InitComm())
        return false;

    if (!InitListeners())
        return false;

    return true;
}

bool WwiseSoundEngine::InitMemoryManager()
{
    //
    // 创建和初始化默认内存管理器。注意，
    // 可以使用您自己的内存管理器覆盖默认内存管理器。请参阅
    // SDK 文档了解更多信息。
    //

    AkMemSettings memSettings;
    memSettings.uMaxNumPools = 20;

    if ( AK::MemoryMgr::Init( &memSettings ) != AK_Success )
    {
        assert( !"Could not create the memory manager." );
        return false;
    }

    return true;
}

bool WwiseSoundEngine::InitStreamMgr()
{
    //
    // 创建并初始化默认 Streaming Manager 实例。注意，
    // 可以使用您自己的流管理器覆盖默认流管理器。请参阅
    // SDK 文档了解更多信息。
    //

    AkStreamMgrSettings stmSettings;
    AK::StreamMgr::GetDefaultSettings(stmSettings);

    // 在此自定义 Stream Manager 设置。

    if (!AK::StreamMgr::Create(stmSettings))
    {
        assert(!"Could not create the Streaming Manager");
        return false;
    }

    if (!m_upWwiseSoundBankMgr->InitLowLevelIO())
        return false;

    return true;
}

bool WwiseSoundEngine::InitSoundEngine()
{
    //
    // 使用默认初始化参数
    // 创建声音引擎
    //

    AkInitSettings initSettings;
    AkPlatformInitSettings platformInitSettings;
    AK::SoundEngine::GetDefaultInitSettings(initSettings);
    AK::SoundEngine::GetDefaultPlatformInitSettings(platformInitSettings);

#ifdef AK_ANDROID
    JNIEnv* env = nullptr;
    JniHelper::getEnv(&env);
    NULL_RETURN_VALUE(env, false);

    jclass classID = env->FindClass("com/example/worldend/game/MainActivity");
    NULL_RETURN_VALUE(classID, false);

    jmethodID methodID = env->GetStaticMethodID(classID, "getInstance", "()Ljava/lang/Object;");
    NULL_RETURN_VALUE(methodID, false);

    platformInitSettings.pJavaVM = JniHelper::getJavaVM();
    platformInitSettings.jNativeActivity = env->CallStaticObjectMethod(classID, methodID);
    if (!m_upWwiseSoundBankMgr->InitAndroidIO(platformInitSettings))
    {
        return false;
    }
#endif

    if (AK::SoundEngine::Init(&initSettings, &platformInitSettings) != AK_Success)
    {
        assert(!"Could not initialize the Sound Engine.");
        return false;
    }

    return true;
}

bool WwiseSoundEngine::InitMusicEngine()
{
    //
    // 使用默认初始化参数
    // 对音乐引擎初始化
    //

    AkMusicSettings musicInit;
    AK::MusicEngine::GetDefaultInitSettings(musicInit);

    if (AK::MusicEngine::Init(&musicInit) != AK_Success)
    {
        assert(!"Could not initialize the Music Engine.");
        return false;
    }

    return true;
}

bool WwiseSoundEngine::InitSpatialAudio()
{
    AkSpatialAudioInitSettings settings;
    settings.uDiffractionFlags = DiffractionFlags_UseBuiltInParam | DiffractionFlags_UseObstruction | DiffractionFlags_CalcEmitterVirtualPosition;
    if (AK::SpatialAudio::Init(settings) != AK_Success)
    {
        return false;
    }
    return true;
}

bool WwiseSoundEngine::InitComm()
{
#ifndef AK_OPTIMIZED
    //
    // 对通信进行初始化（在发布版本中无此步骤！）
    //
    AkCommSettings commSettings;
    AK::Comm::GetDefaultInitSettings(commSettings);
    AKRESULT res = AK::Comm::Init(commSettings);
    if (res != AK_Success)
    {
        assert(!"Could not initialize communication.");
        return false;
    }
#endif // AK_OPTIMIZED
    return true;
}

bool WwiseSoundEngine::InitListeners()
{
    // 注册主要听者。
    RegisterGameObj(MY_DEFAULT_LISTENER);

    // 将一个听者设置为默认。
    AKRESULT res = AK::SoundEngine::SetDefaultListeners(&MY_DEFAULT_LISTENER, 1);

    return res == AK_Success;
}

bool WwiseSoundEngine::RegisterGameObj(AkGameObjectID in_gameObjectID)
{
    AKRESULT res = AK::SoundEngine::RegisterGameObj(in_gameObjectID);
    assert(res == AK_Success);
    return true;
}
