#pragma once

#include <Singleton.h>
#include <SoundSystem/SoundSystem.h>
#include "WwiseSoundBankMgr.h"
#include "WwiseDefine.h"
#include <memory>

//class CAkFilePackageLowLevelIOBlocking;

class WwiseSoundEngine : public SoundSystem
{
public:
    WwiseSoundEngine();
    ~WwiseSoundEngine();

    bool Init() override ;
    bool Run(int timeInterval) override ;
    bool Shut() override ;

    bool Play(const char* name) override;
    bool Stop(const char* name) override;

    AkPlayingID PostEvent(const char* in_pszEventName, AkGameObjectID in_gameObjectID = MY_DEFAULT_LISTENER);

private:
    bool InitWwiseSoundEngine();
    //初始化 Memory Manager
    bool InitMemoryManager();
    //初始化StreamMgr
    bool InitStreamMgr();
    //初始化声音引擎
    bool InitSoundEngine();
    //初始化音乐引擎
    bool InitMusicEngine();
    bool InitSpatialAudio();
    //通信初始化
    bool InitComm();
    bool InitListeners();

    bool RegisterGameObj(AkGameObjectID in_gameObjectID);

    std::unique_ptr<WwiseSoundBankMgr> m_upWwiseSoundBankMgr;
};