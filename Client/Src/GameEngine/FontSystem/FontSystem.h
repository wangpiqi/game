#pragma once

#include "../Object/Object.h"

class FontSystem : public Object
{
public:
    virtual ~FontSystem() {};

    virtual bool Init() override;
    virtual bool Run(int timeInterval) override;
    virtual bool Shut() override;

    virtual void SetText(const std::string& text) {};
};