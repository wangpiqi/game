#include "FreeType.h"

#define WIDTH   640
#define HEIGHT  480

/* origin is the upper left corner */
unsigned char image[HEIGHT][WIDTH];

bool FreeType::Init()
{
    auto error = FT_Init_FreeType(&m_library);
    if (error)
    {
        //... an error occurred during library initialization ...
        return false;
    }

    error = FT_New_Face(m_library,
        "Font/arial.ttf",
        0,
        &m_face);
    if (error == FT_Err_Unknown_File_Format)
    {
        //... the font file could be opened and read, but it appears
            //... that its font format is unsupported
        return false;
    }
    else if (error)
    {
        //... another error code means that the font file could not
            //... be opened or read, or that it is broken...
        return false;
    }

    error = FT_Set_Char_Size(
        m_face,    /* handle to face object           */
        0,       /* char_width in 1/64th of points  */
        50 * 64,   /* char_height in 1/64th of points */
        100,     /* horizontal device resolution    */
        100);   /* vertical device resolution      */

    //error = FT_Set_Pixel_Sizes(
    //    m_face,   /* handle to face object */
    //    0,      /* pixel_width           */
    //    16);   /* pixel_height          */

    //

    /*auto charcode{ 0x1F028 };
    auto glyph_index = FT_Get_Char_Index(m_face, 'A');
    if (glyph_index == 0)
        return false;*/

    //error = FT_Load_Glyph(
    //    m_face,          /* handle to face object */
    //    glyph_index,   /* glyph index           */
    //    FT_LOAD_DEFAULT);  /* load flags, see below */

    //error = FT_Select_Charmap(
    //    m_face,               /* target face object */
    //    FT_ENCODING_BIG5); /* encoding           */

    //error = FT_Render_Glyph(m_face->glyph,   /* glyph slot  */
    //    FT_RENDER_MODE_NORMAL); /* render mode */

    //

    return false;
}

bool FreeType::Run(int timeInterval)
{
    return false;
}

bool FreeType::Shut()
{
    FT_Done_Face(m_face);
    FT_Done_FreeType(m_library);
    return false;
}

void FreeType::SetText(const std::string & text)
{
    for (const auto& c : text)
    {
        auto error = FT_Load_Char(m_face, c, FT_LOAD_RENDER);
        draw_bitmap(&m_face->glyph->bitmap,
            m_face->glyph->bitmap_left,
            HEIGHT - m_face->glyph->bitmap_top);
    }

    show_image();
}

void FreeType::draw_bitmap(FT_Bitmap* bitmap, FT_Int x, FT_Int y)
{
    FT_Int  i, j, p, q;
    FT_Int  x_max = x + bitmap->width;
    FT_Int  y_max = y + bitmap->rows;


    /* for simplicity, we assume that `bitmap->pixel_mode' */
    /* is `FT_PIXEL_MODE_GRAY' (i.e., not a bitmap font)   */

    for (i = x, p = 0; i < x_max; i++, p++)
    {
        for (j = y, q = 0; j < y_max; j++, q++)
        {
            if (i < 0 || j < 0 ||
                i >= WIDTH || j >= HEIGHT)
                continue;

            image[j][i] |= bitmap->buffer[q * bitmap->width + p];
        }
    }
}

void FreeType::show_image(void)
{
    int  i, j;


    for (i = 0; i < HEIGHT; i++)
    {
        for (j = 0; j < WIDTH; j++)
            putchar(image[i][j] == 0 ? ' '
                : image[i][j] < 128 ? '+'
                : '*');
        putchar('\n');
    }
}
