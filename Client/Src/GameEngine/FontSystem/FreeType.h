#pragma once

#include "FontSystem.h"
#include <ft2build.h>
#include <string>
#include FT_FREETYPE_H

class FreeType final : public FontSystem
{
public:
    virtual bool Init() override;
    virtual bool Run(int timeInterval) override;
    virtual bool Shut() override;

    virtual void SetText(const std::string& text) override;

private:
    void draw_bitmap(FT_Bitmap* bitmap, FT_Int x, FT_Int y);
    void show_image(void);

private:
    FT_Library  m_library;
    FT_Face     m_face;
};