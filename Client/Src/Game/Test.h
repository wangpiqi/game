//
// Created by worldend on 2018/11/10.
//

#ifndef ANDROID_TEST_H
#define ANDROID_TEST_H


#include <string>
#include "MacroUtility.h"
#include "Object/Object.h"
#include "Event/EventManager.h"
#include "Event/Event.h"

class ObjectTest : public Object
{
public:
    //
};

class ObjectA : public Object
{
public:
	ObjectA() 
	{
		REGISTER_EVENT(EVENT_ID::EVENT_ID_TEST, ObjectA::funcCallBack);
	};

	~ObjectA()
	{
		UNREGISTER_EVENT(EVENT_ID::EVENT_ID_TEST, ObjectA::funcCallBack);
	}

private:
	bool funcCallBack(const Event & event);
};

class ObjectB : public Object
{
public:
	ObjectB() 
	{
		Event event;
		event.m_EventId = EVENT_ID::EVENT_ID_TEST;
		event.m_Args << "Hello World!";
		EventManager::GetSingleton().postEvent(event);
	};
};

class DLL_EXPORT Test {
public:
    static std::string stringFromJNI();

private:
    static std::string TestStringUtility();
    static std::string TestAlgorithm();
    static std::string TestList();
    static std::string TestContainer();
    static std::string TestBoost();
    static std::string TestTypeTraits();
    static std::string TestMyString();
    static std::string TestBinaryTree();
    static std::string TestVarList();
    static void TestPlaySound();
    static std::string TestLeetCode();
    static std::string TestTinyXml();
    static void TestFreeType();
    static void TestObject();
    static void TestPlayerPrefs();
	static void TestCommon();
	static void TestLua();
};


#endif //ANDROID_TEST_H
