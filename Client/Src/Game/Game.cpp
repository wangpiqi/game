//
// Created by worldend on 2018/11/6.
//
#ifdef ANDROID
#include <Platform/Android/JniHelper.h>
#endif
#include "Game.h"
#include "GameEngine.h"

bool Game::InitGame(VarList& args)
{
    GameEngine::GetSingleton().InitEngine(args);
    return true;
}

bool Game::RunGame(int timeInterval)
{
    GameEngine::GetSingleton().RunEngine(timeInterval);
    return true;
}

bool Game::ShutGame()
{
    GameEngine::GetSingleton().ShutEngine();
    return true;
}

#ifdef ANDROID
bool Game::InitAssetManager(jobject assetManager)
{
    GameEngine::GetSingleton().InitAssetManager(assetManager);
    return true;
}

bool Game::SurfaceCreated(jobject surface)
{
    JniHelper::setSurface(surface);
    return true;
}
#endif

