//
// Created by worldend on 2018/11/10.
//
#include <boost/any.hpp>
#include <StringUtility.h>
#include <Algorithm.h>
#include <List.h>
#include <Container.h>
#include <list>
#include "Test.h"
#include <type_traits>
#include <MyString.h>
#include <BinaryTree.h>
#include <VarList.h>
#include "GameEngine.h"
#include "SoundSystem/SoundSystem.h"
#include "MacroUtility.h"
#include "LeetCode.h"
#include "../tinyxml/tinyxml.h"
#include <iostream>
#include "FontSystem/FontSystem.h"
#include "PlayerPrefs.h"

std::string Test::stringFromJNI()
{
//    std::string str = Test::TestStringUtility();
//    std::string str = Test::TestAlgorithm();
//    std::string str = Test::TestList();
//    std::string str = Test::TestContainer();
//    std::string str = Test::TestBoost();
//    std::string str = Test::TestMyString();
//    std::string str = Test::TestBinaryTree();
    TestPlaySound();
    //std::string str = Test::TestVarList();
    std::string str = Test::TestLeetCode();
    //std::string str = Test::TestTinyXml();
    //TestFreeType();
    //TestObject();
    //TestPlayerPrefs();
	//TestCommon();
    Test::TestLua();
    return str;
}

std::string Test::TestStringUtility()
{
    std::string str = "Hello from C++";
    StringVec temp;
    StringUtility::SplitString(temp, str.c_str(), " ");
    str = StringUtility::CombineString(temp, "@@");
    return str;
}

std::string Test::TestAlgorithm()
{
//    vector<int> input {5,2,4,6,1,3};
//    vector<int> output = Algorithm::InsertionSort(input);
//    vector<int> left {2,4,5,7};
//    vector<int> right {1,2,3,6};
//    vector<int> output = Algorithm::MergeSort(left, right);
//    vector<int> input {-2,1,-3,4,-1,2,1,-5,4};
//    vector<int> input {-1,-2};
//    vector<int> output = Algorithm::FindMaxSubArray(input, 1, input.size());
    int n = Algorithm::BinarySearch(1000, 50);
    vector<int> output {n};
    return StringUtility::CombineString(output);
}

std::string Test::TestList()
{
    List list;
    list.add(1);
    list.add(1);
//    list.add(2);
//    list.add(3);
//    list.add(4);
//    list.add(5);
//    list.reverse();
//    list.add(6);
    while (list.find(1))
    {
        list.remove(1);
    }
//    list.remove(3);
//    list.add(6);
    return list.ToString();
}

std::string Test::TestContainer()
{
    Container<int, list> c;
    c.add(1);
    c.add(2);
    c.add(3);
    c.add(4);
    c.add(5);
    return StringUtility::IntToString(c.get(2));
}

std::string Test::TestBoost()
{
    std::string str = TestTypeTraits();
    return str;
}

std::string Test::TestTypeTraits()
{
    vector<int> output {std::is_integral<int>::value};
    return StringUtility::CombineString(output);
}

std::string Test::TestMyString()
{
    MyString s1;
    return "";
}

std::string Test::TestBinaryTree()
{
    StringVec temp{"1","2","5","3","4","null","6"};
    BinaryTree tree(temp);
    return "";
}

std::string Test::TestVarList()
{
    VarList list;
    list << 2019 << 2 << 4 << 3.14f << "Hello World!";
    StringVec temp{StringUtility::IntToString(list.get<int>(0)), StringUtility::IntToString(list.get<int>(1)), StringUtility::IntToString(list.get<int>(2))};
    temp.emplace_back(StringUtility::FloatToString(list.get<float>(3)));
    temp.emplace_back(list.get<std::string>(4));
    return StringUtility::CombineString(temp, " ");
}

void Test::TestPlaySound()
{
    auto pSoundSystem = GameEngine::GetSingleton().GetSoundSystem();
    NULL_RETURN(pSoundSystem);
    //pSoundSystem->Play("IM_Explore");
    pSoundSystem->Play("Play_music_desert_day");
}

std::string Test::TestLeetCode()
{
    //auto&& result = LeetCode::bitwiseComplement(5);
    //auto&& result = LeetCode::reverseOnlyLetters("ab-cd");
    //vector<int> prices{ 7,1,5,3,6,4 };
    //vector<int> prices{ 1,2 };
    //vector<int> prices{ 2,1,4 };
    //vector<int> prices{ 6,1,3,2,4,7 };
    //vector<int> prices{ 7,1,5,3,6,4 };
    //vector<int> nums{ 3, 6, 1, 0 };
    //vector<int> nums{ 4,3,2,7,8,2,3,1 };
    //vector<int> A{ 1,2,0,0 };
    //vector<int> nums{ 3,2,3,4,6,5 };
    //vector<int> nums{ 1,5,3,2,2,7,6,4,8,9 };
    //std::vector<int> time{ 30,20,150,100,40 };
    //vector<vector<int>> points{ { 1,3 },{ -2,2 } };
	//vector<vector<int>> costs{ {10, 20},{30, 200},{400, 50},{30, 20} };
	//vector<int> heights{ 1,1,4,2,1,3 };
	//std::vector<char> chars{ 'a', 'a', 'b', 'b', 'c', 'c', 'c' };
	//vector<int> nums{ 1, 2, 3, 4, 5 };
    //auto&& result = LeetCode::maxProfit2(prices);
    //auto&& result = LeetCode::dominantIndex(nums);
    //auto&& result = LeetCode::findDuplicates(nums);
    //auto&& result = LeetCode::prefixesDivBy5(A);
    //auto&& result = LeetCode::findLHS(nums);
    //auto&& result = LeetCode::findDisappearedNumbers(nums);
    //auto&& result = LeetCode::canThreePartsEqualSum(A);
    //auto&& result = LeetCode::largestSumAfterKNegations(A, 1);
    //auto&& result = LeetCode::addToArrayForm(A, 34);
    //auto&& result = LeetCode::addBinary("11", "1");
    //auto&& result = LeetCode::singleNonDuplicate(nums);
    //auto&& result = LeetCode::countPrimes(499979);
    //auto&& result = LeetCode::findErrorNums(nums);
    //auto&& result = LeetCode::buddyStrings("abab", "abab");
    //auto&& result = LeetCode::trailingZeroes(1808548329);
    //auto&& result = LeetCode::findNthDigit(1000000000);
    //auto&& result = LeetCode::numPairsDivisibleBy60(time);
    //auto&& result = LeetCode::kClosest(points, 1);
	//auto&& result = LeetCode::twoCitySchedCost(costs);
	//auto&& result = LeetCode::heightChecker(heights);
	//auto&& result = StringUtility::compress(chars);
	//auto&& result = LeetCode::findPairs(nums, -1);
	//auto&& result = LeetCode::findShortestSubArray(IntVec{ 1,2,2,3,1,4,2 });
	//auto&& result = LeetCode::pivotIndex(IntVec{ 1, 7, 3, 6, 5, 6 });
	/*auto&& result = LeetCode::repeatedStringMatch("bb",
		"bbbbbbb");*/
    //auto&& result = LeetCode::validPalindrome("abc");
    //auto&& result = LeetCode::minMoves2(IntVec{ 1, 2, 3 });
    //auto&& result = LeetCode::removeOuterParentheses("(()())(())");
    //auto result = LeetCode::calPoints(StringVec{ "5","2","C","D","+" });
    //auto&& result = LeetCode::relativeSortArray(IntVec{ 2,3,1,3,2,4,6,7,9,2,19 }, IntVec{ 2,1,4,3,9,6 });
	/*vector<vector<char>> board {
		{'.','.','.','.','.','.','.','.'},
		{'.','.','.','p','.','.','.','.'},
		{'.','.','.','R','.','.','.','p'},
		{'.','.','.','.','.','.','.','.'},
		{'.','.','.','.','.','.','.','.'},
		{'.','.','.','p','.','.','.','.'},
		{'.','.','.','.','.','.','.','.'},
		{'.','.','.','.','.','.','.','.'} };
	auto&& result = LeetCode::numRookCaptures(board);*/
	//auto&& result = LeetCode::findOcurrences("alice is a good girl she is a good student", "a", "good");
	//auto&& result = LeetCode::robotSim(IntVec{ 4,-1,3 }, vector<IntVec>{});
	//auto&& result = LeetCode::validMountainArray(IntVec{ 2,1 });
	//auto&& result = LeetCode::largeGroupPositions("aaa");
	//auto&& result = LeetCode::dayOfYear("2019-01-09");
	auto&& result = LeetCode::countAndSay(4);
    //return StringUtility::IntToString(result);
    //return StringUtility::CombineString(result);
    return result;
    //return "";
}

std::string Test::TestTinyXml()
{
    TiXmlDocument doc("Config/utf8test.xml");
    if (!doc.LoadFile())
        return "";

    auto&& pRoot = doc.RootElement();
    NULL_RETURN_VALUE(pRoot, "");

    auto&& pElement = pRoot->FirstChildElement();
    cout << pElement->GetText() << endl
        << pElement->Value() << endl
        << pElement->FirstAttribute()->Name() << endl
        << pElement->FirstAttribute()->Value() <<endl;

    return "";
}

void Test::TestFreeType()
{
    auto pFontSystem = /*dynamic_cast<FreeType*>*/(GameEngine::GetSingleton().GetFontSystem());
    pFontSystem->SetText("A");
}

void Test::TestObject()
{
    ObjectTest objTest;
    objTest.SetProperty<std::string>("Property1", "Hello World!");
    objTest.SetProperty<std::string>("Property1", "HaHaHa");
    objTest.SetProperty<int>("Property1", 11037);
    auto&& property = objTest.GetProperty<std::string>("Property1");
}

void Test::TestPlayerPrefs()
{
    PlayerPrefs::SetInt("key1", 11037);
    PlayerPrefs::SetInt("key1", 2019);
    PlayerPrefs::SetString("key1", "Hello");
    auto&& value1 = PlayerPrefs::GetInt("key1");
}

void Test::TestCommon()
{
	cout<<sizeof(Object)<<" "<<sizeof(boost::any)<<endl;

	ObjectA* pObjectA = new ObjectA();
	delete pObjectA;
	ObjectB objectB;
}

void Test::TestLua()
{
    //
}

bool ObjectA::funcCallBack(const Event & event)
{
	cout<<"funcCallBack"<<endl;
	cout << event.m_Args.get<std::string>(0).c_str() << endl;
	return false;
}
