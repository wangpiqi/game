#ifndef WIN32

#include <jni.h>
#include <string>
#include <StringUtility.h>
#include <Test.h>
#include "Game.h"
#include <VarList.h>
#include <Platform/Android/JniHelper.h>
#include <android/asset_manager_jni.h>

#ifndef JAVAFUNC_ARG_OBJ
#define JAVAFUNC_ARG_OBJ JNIEnv* env, jobject obj
#endif

#ifndef MAKE_JAVAFUNC
#define MAKE_JAVAFUNC(ret_type, func_name, args)\
extern "C" JNIEXPORT ret_type JNICALL Java_com_example_worldend_game_WrapJNI_##func_name args
#endif

MAKE_JAVAFUNC(jstring, stringFromJNI, (JAVAFUNC_ARG_OBJ))
{
    std::string str{};
    str = Test::stringFromJNI();
    return env->NewStringUTF(str.c_str());
}

MAKE_JAVAFUNC(jboolean, InitGame, (JAVAFUNC_ARG_OBJ, int width, int height))
{
    jboolean result = Game::GetSingleton().InitGame(VarList()
            << 0 << width << height);
    return result;
}

MAKE_JAVAFUNC(jboolean, RunGame, (JAVAFUNC_ARG_OBJ))
{
    jboolean result{};

    static auto time1 = clock();
    auto time2 = clock();
    int timeInterval = ((time2 - time1) / CLOCKS_PER_SEC) * 1000; //毫秒
    if (timeInterval > 0)
    {
        result = Game::GetSingleton().RunGame(timeInterval);
        time1 = time2;
    }

    return result;
}

MAKE_JAVAFUNC(jboolean, ShutGame, (JAVAFUNC_ARG_OBJ))
{
    jboolean result = Game::GetSingleton().ShutGame();
    return result;
}

MAKE_JAVAFUNC(jboolean, InitAssetManager, (JAVAFUNC_ARG_OBJ, jobject assetManager))
{
    auto mgr = AAssetManager_fromJava(env, assetManager);
    JniHelper::setAssetManager(mgr);
    return true;
}

MAKE_JAVAFUNC(jboolean, SurfaceCreated, (JAVAFUNC_ARG_OBJ, jobject surface))
{
    jboolean result = Game::GetSingleton().SurfaceCreated(surface);
    return result;
}

MAKE_JAVAFUNC(jboolean, SurfaceChanged, (JAVAFUNC_ARG_OBJ, int width, int height))
{
    jboolean result = Game::GetSingleton().SurfaceChanged(width, height);
    return result;
}

#endif // !WIN32

