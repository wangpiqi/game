#include "stdafx.h"
#include <Windows.h>
#include "..\..\..\Game.h"
#include "..\..\..\Test.h"
#include "LogUtility.h"
#include <time.h>
#ifdef _WIN32
#include "..\..\..\..\GameEngine\Platform\Windows\ConsoleUtility.h"
#endif

#define CLASS_NAME L"Game"
#define WINDOW_NAME L"Game"

#define WINDOW_WIDTH 1024
#define WINDOW_HEIGHT 768

HWND g_hwnd{};

LRESULT CALLBACK WndProc(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    switch (message)
    {
    case WM_CREATE:
        return 0;
    case WM_DESTROY:
    {
        PostQuitMessage(0);
        return 0;
    }
    break;
    }
    return DefWindowProc(hwnd, message, wParam, lParam);
}

int WINAPI WinMain(HINSTANCE hInstance,HINSTANCE hPrevInstance,LPSTR lpCmdLine,int nCmdShow)
{
#ifndef NDEBUG
    ConsoleUtility::CreateConsole();
#endif

    WNDCLASS cls;
    cls.cbClsExtra = 0;
    cls.cbWndExtra = 0;
    cls.hbrBackground = NULL;
    cls.hCursor = NULL;
    cls.hIcon = NULL;
    cls.hInstance = hInstance;
    cls.lpfnWndProc = WndProc;
    cls.lpszClassName = CLASS_NAME;
    cls.lpszMenuName = L"";
    cls.style = 0;

    RegisterClass(&cls);

	g_hwnd = CreateWindow(CLASS_NAME,      // window class name
        WINDOW_NAME,   // window caption
        WS_OVERLAPPEDWINDOW,  // window style
        CW_USEDEFAULT,// initial x position
        CW_USEDEFAULT,// initial y position
		WINDOW_WIDTH,// initial x size
		WINDOW_HEIGHT,// initial y size
        NULL,                 // parent window handle
        NULL,            // window menu handle
        hInstance,   // program instance handle
        NULL);      // creation parameters

    ShowWindow(g_hwnd, SW_SHOW);

    UpdateWindow(g_hwnd);

	//WindowsHelper::setHWND(g_hwnd);

    Game::GetSingleton().InitGame(VarList() << g_hwnd << WINDOW_WIDTH << WINDOW_HEIGHT);

#ifndef NDEBUG
    auto&& strFromJNI = Test::stringFromJNI();
    LOGI(strFromJNI.c_str());
#endif

    MSG msg{};
    while (msg.message != WM_QUIT)
    {
        static auto time1 = clock();
        if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
        {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
        else
        {
            auto time2 = clock();
            int timeInterval = ((time2 - time1) / CLOCKS_PER_SEC) * 1000; //毫秒
            if (timeInterval > 0)
            {
                Game::GetSingleton().RunGame(timeInterval);
                time1 = time2;
            }
        }
    }

    Game::GetSingleton().ShutGame();

    UnregisterClass(CLASS_NAME, hInstance);

#ifndef NDEBUG
    ConsoleUtility::DestroyConsole();
#endif

    return 0;
}