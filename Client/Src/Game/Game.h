//
// Created by worldend on 2018/11/6.
//
#ifndef ANDROID_GAME_H
#define ANDROID_GAME_H

#include "Singleton.h"
#include "MacroUtility.h"
#include "VarList.h"
#ifdef ANDROID
#include <jni.h>
#endif

class DLL_EXPORT Game : public Singleton<Game>
{
public:
    Game() {};

public:
    bool InitGame(VarList& args);
    bool RunGame(int timeInterval);
    bool ShutGame();

#ifdef ANDROID
    bool InitAssetManager(jobject assetManager);

	bool SurfaceCreated(jobject surface);
	bool SurfaceChanged(int width, int height) { return true; };
#endif
};
#endif //ANDROID_GAME_H
