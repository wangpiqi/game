@echo off
set SRC=%~dp0

set ASSETS_PATH=.
set MAIN_ASSETS_PATH=..\Android\app\src\main\assets

call :func_DELFILE %MAIN_ASSETS_PATH%

call :func_COPYFILE Resource

pause

rem --------------------------------------------------
rem call :func_MKDIR
rem --------------------------------------------------
:func_MKDIR
@if not exist %1 @mkdir %1
exit /b

rem --------------------------------------------------
rem call :func_COPYFILE
rem --------------------------------------------------
:func_COPYFILE
    xcopy /y /d /s /i %ASSETS_PATH%\%1 %MAIN_ASSETS_PATH%\%1
exit /b

:func_COPYFILE_ALL
    xcopy /y /d /s /i %ASSETS_PATH% %MAIN_ASSETS_PATH%
exit /b

rem --------------------------------------------------
rem call :func_DELFILE
rem --------------------------------------------------
:func_DELFILE
	rd /s /q  %1
exit /b