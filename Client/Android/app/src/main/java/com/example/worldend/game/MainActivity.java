package com.example.worldend.game;

import android.app.Activity;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.view.Surface;
import android.widget.TextView;
import com.example.worldend.OpenGL.OpenGLView;
import com.tencent.bugly.crashreport.CrashReport;

public class MainActivity extends Activity {
    private static Object m_instance;
    private ConstraintLayout m_Layout;
    private OpenGLView m_OpenGLView;

    public static Object getInstance() {
        return m_instance;
    }

    public OpenGLView getOpenGLView() {
        return m_OpenGLView;
    }

    public static Surface getSurface() {
        return ((MainActivity)m_instance).getOpenGLView().getSurface();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        m_instance = this;

        super.onCreate(savedInstanceState);

        CrashReport.initCrashReport(getApplicationContext(), "a65ba625aa", true);

        WrapJNI.InitAssetManager(getAssets());

        m_OpenGLView = new OpenGLView(getApplication());

        setContentView(R.layout.activity_main);
        m_Layout = (ConstraintLayout) findViewById(R.id.ConstraintLayout); //new ConstraintLayout(this);
        m_Layout.addView(m_OpenGLView);

//        WrapJNI.InitGame(800, 600);

        TestTextView();
    }

//    @Override
//    public void run() {
//        WrapJNI.RunGame();
//    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        WrapJNI.ShutGame();
    }

    void TestTextView()
    {
        // Example of a call to a native method
        TextView tv = (TextView) findViewById(R.id.sample_text);
        String str = WrapJNI.stringFromJNI();
        if (tv != null)
        {
            tv.setText(str);
        }
    }
}
