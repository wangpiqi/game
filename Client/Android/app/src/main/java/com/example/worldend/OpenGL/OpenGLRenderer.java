package com.example.worldend.OpenGL;

import android.opengl.GLSurfaceView;
import com.example.worldend.game.WrapJNI;
import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import static com.example.worldend.game.MainActivity.getSurface;

public class OpenGLRenderer implements GLSurfaceView.Renderer
{
    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig config)
    {
        WrapJNI.SurfaceCreated(getSurface());
        WrapJNI.InitGame(800, 600);
    }

    @Override
    public void onSurfaceChanged(GL10 gl, int width, int height)
    {
        WrapJNI.SurfaceChanged(width, height);
    }

    @Override
    public void onDrawFrame(GL10 gl)
    {
        WrapJNI.RunGame();
    }
}