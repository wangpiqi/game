package com.example.worldend.game;

import android.content.res.AssetManager;
import android.view.Surface;

public class WrapJNI {
    // Used to load the 'native-lib' library on application startup.
    static {
        System.loadLibrary("GameEngine");
        System.loadLibrary("Game");
    }

    /**
     * A native method that is implemented by the 'native-lib' native library,
     * which is packaged with this application.
     */
    public static native String stringFromJNI();

    public static native boolean InitGame(int width, int height);
    public static native boolean RunGame();
    public static native boolean ShutGame();

    public static native boolean InitAssetManager(AssetManager assetManager);

    public static native boolean SurfaceCreated(Surface surface);
    public static native boolean SurfaceChanged(int width, int height);
}
