package com.example.worldend.OpenGL;

import android.content.Context;
import android.opengl.GLSurfaceView;
import android.view.Surface;

public class OpenGLView extends GLSurfaceView
{
    OpenGLRenderer m_OpenGLRenderer;

    public OpenGLView(Context context)
    {
        super(context);
        init();
    }

    public Surface getSurface() {
        return getHolder().getSurface();
    }

    private void init() {
        setEGLContextClientVersion(3);
        m_OpenGLRenderer = new OpenGLRenderer();
        setRenderer(m_OpenGLRenderer);
    }
}
