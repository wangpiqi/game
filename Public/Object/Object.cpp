//
// Created by worldend on 2019/1/20.
//

#include "Object.h"
#include "MacroUtility.h"

Object::~Object()
{
    for (auto it = m_vecComponent.begin();it != m_vecComponent.end();++it)
    {
        SAFE_DELETE(*it);
    }
}

bool Object::Update(int timeInterval)
{
    Run(timeInterval);
    for (auto it = m_vecComponent.begin();it != m_vecComponent.end();++it)
    {
        (*it)->Run(timeInterval);
    }
    return false;
}

void Object::AddComponent(Component * pComponent)
{
    NULL_RETURN(pComponent);
    m_vecComponent.emplace_back(pComponent);
    pComponent->Init(this);
}

void Object::RemoveComponent(Component * pComponent)
{
    NULL_RETURN(pComponent);
    for (auto it = m_vecComponent.begin();it != m_vecComponent.end();++it)
    {
        if (*it == pComponent)
        {
            m_vecComponent.erase(it);
            pComponent->Shut();
            return;
        }
    }
}
