//
// Created by worldend on 2019/1/20.
//
#include <MacroUtility.h>
#include <algorithm>
#include "ObjectMgr.h"

ObjectMgr::ObjectMgr()
{
    //
}

ObjectMgr::~ObjectMgr()
{
    for (auto obj : m_ObjectPtrVec)
    {
        obj->Shut();
        SAFE_DELETE(obj);
    }
}

Object* ObjectMgr::AddObject(Object* obj)
{
    auto it = std::find(m_ObjectPtrVec.begin(), m_ObjectPtrVec.end(), obj);
    if (it != m_ObjectPtrVec.end())
        return *it;

    obj->Init();
    m_ObjectPtrVec.emplace_back(obj);
    return obj;
}

void ObjectMgr::RemoveObject(Object* obj)
{
    auto it = std::find(m_ObjectPtrVec.begin(), m_ObjectPtrVec.end(), obj);
    if (it != m_ObjectPtrVec.end())
    {
        obj->Shut();
        m_ObjectPtrVec.erase(it);
    }
}

//Object* ObjectMgr::FindObject(Object* obj)
//{
//    auto it = std::find(m_ObjectPtrVec.begin(), m_ObjectPtrVec.end(), obj);
//    if (it != m_ObjectPtrVec.end())
//        return *it;
//    return nullptr;
//}

Object * ObjectMgr::FindObject(const char * name)
{
    for (auto&& obj : m_ObjectPtrVec)
    {
        if (strcmp(obj->GetProperty<std::string>("Name").c_str(), name) == 0)
        {
            return obj;
        }
    }
    return nullptr;
}

bool ObjectMgr::Update(int timeInterval)
{
    for (auto obj : m_ObjectPtrVec)
    {
        obj->Update(timeInterval);
    }
    return true;
}

