//
// Created by worldend on 2019/1/20.
//
#ifndef ANDROID_OBJECT_H
#define ANDROID_OBJECT_H

#include "IObject.h"
#include <assert.h>
#include "../Component/Component.h"
#include "MacroUtility.h"
#include "../Event/Event.h"
#include "../Event/EventManager.h"
#include <string>
#include <any>
#include <vector>

class Object : public IObject {
public:
    virtual ~Object();

    virtual bool Init() override { return true; };
    bool Update(int timeInterval);
    virtual bool Run(int timeInterval) override { return true; };
    virtual bool Shut() override { return true; };

	DECLARE_PROPERTY_MAP;

    void AddComponent(Component* pComponent);
    void RemoveComponent(Component* pComponent);

protected:
    std::vector<Component*> m_vecComponent;
};
#endif //ANDROID_OBJECT_H
