//
// Created by worldend on 2019/1/20.
//

#ifndef ANDROID_OBJECTMGR_H
#define ANDROID_OBJECTMGR_H

#include <vector>
#include "Object.h"

using ObjectPtrVec = std::vector<Object*>;

class ObjectMgr {
public:
    ObjectMgr();
    ~ObjectMgr();

    Object* AddObject(Object* obj);
    void RemoveObject(Object* obj);
    //Object* FindObject(Object* obj);
    Object* FindObject(const char* name);

    bool Update(int timeInterval);

private:
    ObjectPtrVec m_ObjectPtrVec;
};


#endif //ANDROID_OBJECTMGR_H
