//
// Created by worldend on 2019/1/20.
//

#ifndef ANDROID_IOBJECT_H
#define ANDROID_IOBJECT_H

class IObject
{
public:
    virtual ~IObject() {};

    virtual bool Init() = 0;
    virtual bool Run(int timeInterval) = 0;
    virtual bool Shut() = 0;
};

#endif //ANDROID_IOBJECT_H
