#include <MacroUtility.h>
#include "Algorithm.h"
#include <limits.h>

vector<int> Algorithm::InsertionSort(IN const vector<int>& input)
{
    vector<int> output;

    if (input.size() == 0)
        return output;

    output.resize(input.size());
    output[0] = input[0];

    for (int i = 1; i < (int)input.size(); ++i)
    {
        int j = i-1;
        while (j >= 0 && output[j] > input[i])
        {
            output[j+1] = output[j];
            --j;
        }
        output[j+1] = input[i];
    }

    return output;
}

vector<int> Algorithm::MergeSort(const vector<int>& left, const vector<int>& right)
{
    vector<int> output;

    int i = 0;
    int j = 0;
    while((i < (int)left.size()) && (j < (int)right.size())) {
        if (left[i] <= right[j])
        {
            output.push_back(left[i]);
            ++i;
        } else
        {
            output.push_back(right[j]);
            ++j;
        }
    }

    if (i == left.size())
    {
        output.insert(output.end(), right.begin() + j, right.end());
    } else
    {
        output.insert(output.end(), left.begin() + i, left.end());
    }

    return output;
}

int Algorithm::BinarySearch(int n, int num)
{
    int low = 1;
    int high = n;

    while (low <= high)
    {
        int mid = low + (high - low) / 2;

        if (mid > num)
            high = mid - 1;
        else if (mid < num)
            low = mid + 1;
        else
            return mid;
    }

    return -1;
}

vector<int> Algorithm::FindMaxCrossingSubArray(const vector<int>& input, int low, int mid, int high)
{
    int left_sum = INT_MIN;
    int sum = 0;
    int max_left = 0;
    for (int i = mid; i >= low; --i) {
        sum += input[i-1];
        if (sum > left_sum)
        {
            left_sum = sum;
            max_left = i;
        }
    }

    int right_sum = INT_MIN;
    sum = 0;
    int max_right = 0;
    for (int i = mid + 1; i <= high; ++i) {
        sum += input[i-1];
        if (sum > right_sum)
        {
            right_sum = sum;
            max_right = i;
        }
    }

    vector<int> output {max_left,max_right,left_sum+right_sum};

    return output;
}

vector<int> Algorithm::FindMaxSubArray(const vector<int>& input, int low, int high)
{
    if (low == high)
    {
        vector<int> output {low,high,input[low-1]};
        return output;
    }

    int mid = (low + high) / 2;

    auto vecLeft = FindMaxSubArray(input, low, mid);
    auto vecRight = FindMaxSubArray(input, mid+1, high);
    auto vecMiddle = FindMaxCrossingSubArray(input, low, mid, high);

    if (vecLeft[2] >= vecRight[2] && vecLeft[2] >= vecMiddle[2])
        return vecLeft;
    else if (vecRight[2] >= vecLeft[2] && vecRight[2] >= vecMiddle[2])
        return vecRight;
    else
        return vecMiddle;
}
