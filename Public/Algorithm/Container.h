//
// Created by worldend on 2018/11/12.
//

#ifndef ANDROID_CONTAINER_H
#define ANDROID_CONTAINER_H

#include <list>
#include <string>
using namespace std;

template <typename T, template <typename U, typename V> typename container>
class Container
{
public:
    void add(const T& value)
    {
        m_container.push_back(value);
    };

    const T& get(int index)
    {
        auto it = m_container.begin();
        for (int i = 0; i < index; ++i) {
            ++it;
        }
        return *it;
    }

    std::string ToString()
    {
        return "";
    }

private:
    container<T, std::allocator<T>> m_container;
};

#endif //ANDROID_CONTAINER_H
