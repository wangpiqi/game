#pragma once

class MyString
{
public:
    MyString();
    MyString(const char* str);
    ~MyString();

    MyString(const MyString& s);
    MyString& operator=(const MyString& s);

private:
    char* m_data;
};