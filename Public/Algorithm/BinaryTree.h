#pragma once

#include <cstdlib>
#include <vector>
#include <string>
#include <StringUtility.h>

struct TreeNode {
     int val;
     TreeNode *left;
     TreeNode *right;
     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};

class BinaryTree
{
public:
    BinaryTree();
    BinaryTree(const StringVec& input);
    ~BinaryTree();

private:
    TreeNode* m_pRoot;
};