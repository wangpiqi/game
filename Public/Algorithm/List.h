#pragma once

#include <cstdlib>
#include <string>
#include <vector>

struct ListNode {
    int val;
    ListNode *next;
    ListNode(int x) : val(x), next(NULL) {}
};

class List
{
public:
    List();
//    List(ListNode* head) : m_head(head)
//    {
//        m_tail = nullptr;
//        while(head)
//        {
//            m_tail = head;
//            head = head->next;
//        }
//    };
    ~List();

    void add(int val);
    bool find(int val);
    void remove(int val);
    void reverse();
    size_t size() { return m_size; };
    std::string ToString();
    std::vector<int> ToVector();
private:
    ListNode* GetTail() { return m_tail; };
    void Destroy();
    void Delete(ListNode* node);

private:
    ListNode* m_head;
    ListNode* m_tail;
    size_t m_size;
};
