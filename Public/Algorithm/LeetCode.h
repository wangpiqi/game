#pragma once

#include <string>
#include <vector>
#include <stack>

using namespace std;

class LeetCode
{
public:
    static int bitwiseComplement(int N);
    static std::string reverseOnlyLetters(std::string S);
    static int maxProfit(std::vector<int>& prices);
    static int maxProfit2(std::vector<int>& prices);
    static int dominantIndex(std::vector<int>& nums);
    static std::vector<int> findDuplicates(std::vector<int>& nums);
    static std::vector<bool> prefixesDivBy5(std::vector<int>& A);
    static int findLHS(std::vector<int>& nums);
    static std::vector<int> findDisappearedNumbers(std::vector<int>& nums);
    static bool canThreePartsEqualSum(std::vector<int>& A);
    static int largestSumAfterKNegations(std::vector<int>& A, int K);
    static std::vector<int> addToArrayForm(std::vector<int>& A, int K);
    static std::string addBinary(std::string a, std::string b);
    static int singleNonDuplicate(std::vector<int>& nums);
    static int countPrimes(int n);
    static vector<int> findErrorNums(vector<int>& nums);
    static bool buddyStrings(string A, string B);
    static int trailingZeroes(int n);
    static int findNthDigit(int n);
    static int numPairsDivisibleBy60(std::vector<int>& time);
    static std::vector<std::vector<int>> kClosest(std::vector<std::vector<int>>& points, int K);
	static int twoCitySchedCost(vector<vector<int>>& costs);
	static int heightChecker(vector<int>& heights);
	static int findPairs(vector<int>& nums, int k);
	static int findShortestSubArray(vector<int>& nums);
	static int pivotIndex(vector<int>& nums);
	static int repeatedStringMatch(string A, string B);
    static bool validPalindrome(string s);
    static int minMoves2(vector<int>& nums);
    static string removeOuterParentheses(string S);
    static int calPoints(vector<string>& ops);
    static vector<int> relativeSortArray(vector<int>& arr1, vector<int>& arr2);
    static int numRookCaptures(vector<vector<char>>& board);
	static vector<string> findOcurrences(string text, string first, string second);
	static int robotSim(vector<int>& commands, vector<vector<int>>& obstacles);
	static bool validMountainArray(vector<int>& A);
	static vector<vector<int>> largeGroupPositions(string S);
	static int dayOfYear(std::string date);
	static bool isLeapYear(int year);
	static int getDayOfMonth(int month, bool leapYear = false);
	static std::string say(std::string str);
	static std::string countAndSay(int n);
private:
    static bool isLetters(const char& c);
    static int maxProfit(std::vector<int>& prices, int start, int end);
    static int maxProfitCross(std::vector<int>& prices, int start, int end, int mid);
    static int canEqualSum(int sum, std::vector<int>& A, int start);
    static std::vector<int> intToArray(int K);
    static int caculateDigits(int n);
    static int distanceSquare(const std::vector<int>& point);
	static std::string printStack(std::stack<char>& s);
	template <class T>
	static void clearStack(std::stack<T>& s) 
	{
		std::stack<T> temp;
		s.swap(temp);
	};
};