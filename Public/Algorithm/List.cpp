#include <MacroUtility.h>
#include <StringUtility.h>
#include "List.h"

List::List()
{
    m_head = nullptr;
    m_tail = nullptr;
    m_size = 0;
}

List::~List()
{
    Destroy();
}

void List::add(int val)
{
    if (m_tail == nullptr)
    {
        m_head = new ListNode(val);
        m_tail = m_head;
    }
    else
    {
        m_tail->next = new ListNode(val);
        m_tail = m_tail->next;
    }

    ++m_size;
}

bool List::find(int val)
{
    ListNode* node = m_head;
    while (node)
    {
        if (node->val == val)
            return true;
        node = node->next;
    }
    return false;
}

void List::remove(int val)
{
    NULL_RETURN(m_head);

    ListNode* node = m_head;
    ListNode* next = nullptr;
    if (m_head->val == val)
    {
        next = m_head->next;
        Delete(m_head);
        m_head = next;

        if (m_head == nullptr)
            m_tail = nullptr;
    }
    else
    {
        while (node)
        {
            next = node->next;
            if (next && next->val == val)
            {
                if (next == m_tail)
                {
                    m_tail = node;
                }

                ListNode* nextnext = next->next;
                Delete(next);
                node->next = nextnext;
                break;
            }
            node = next;
        }
    }
}

void List::reverse()
{
    m_tail = m_head;

    ListNode* prev = nullptr;
    ListNode* next = nullptr;

    while (m_head)
    {
        next = m_head->next;
        m_head->next = prev;
        prev = m_head;
        m_head = next;
    }

    m_head = prev;
}

std::string List::ToString()
{
    std::string output = "data:";

    ListNode* node = m_head;
    while (node)
    {
        output += StringUtility::IntToString(node->val) + ",";
        node = node->next;
    }

    output = output.substr(0, output.length() - 1);
    output += "\nsize:" + StringUtility::IntToString(size());

    return output;
}

std::vector<int> List::ToVector()
{
    std::vector<int> output;

    ListNode* node = m_head;
    while (node)
    {
        output.push_back(node->val);
        node = node->next;
    }

    return output;
}

void List::Destroy()
{
    while (m_head)
    {
        ListNode *next = m_head->next;
        Delete(m_head);
        m_head = next;
    }

    m_head = nullptr;
    m_tail = nullptr;
//    m_size = 0;
}

void List::Delete(ListNode* node)
{
    SAFE_DELETE(node);
    --m_size;
}