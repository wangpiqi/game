#include "LeetCode.h"
#include <bitset>
#include "Algorithm.h"
#include <algorithm>
#include <iostream>
#include "StringUtility.h"
#include <map>
#include <set>
#include <cmath>
#include <numeric>
#include "LogUtility.h"
#include <stack>

int LeetCode::bitwiseComplement(int N)
{
    if (N == 0)
        return 1;

    std::bitset<32> bs(N);

    bool bStart{ false };
    for (int i = bs.size() - 1;i >= 0;--i)
    {
        if (bs.test(i))
            bStart = true;

        if (!bStart)
            continue;

        bs.flip(i);
    }

    return bs.to_ulong();
}

std::string LeetCode::reverseOnlyLetters(std::string S)
{
    if (S.size() < 2)
        return S;

    int start = 0;
    int end = S.size() - 1;
    while (start < end)
    {
        if (!isLetters(S[start]))
            ++start;
        else if (!isLetters(S[end]))
            --end;
        else
        {
            Algorithm::Swap<char>(S[start], S[end]);  ++start;  --end;
        }
    }

    return S;
}

int LeetCode::maxProfit(std::vector<int>& prices)
{
    return maxProfit(prices, 0, prices.size() - 1);
}

int condition()
{
    return 0;
}

int LeetCode::maxProfit2(std::vector<int>& prices)
{
    int maxProfit2{};
    for (int i{};i < (int)prices.size() - 1;++i)
    {
        int profit = prices[i + 1] - prices[i];
        if (profit > 0)
            maxProfit2 += profit;
    }
    return maxProfit2;
}

int LeetCode::dominantIndex(std::vector<int>& nums)
{
    if (nums.empty())
        return -1;

    auto&& it = std::max_element(nums.begin(), nums.end());

    int&& index = it - nums.begin();

    for (int i = 0;i < (int)nums.size();++i)
    {
        if (i == index)
            continue;

        if (nums[i] * 2 > *it)
            return -1;
    }

    return index;
}

std::vector<int> LeetCode::findDuplicates(std::vector<int>& nums)
{
    if (nums.empty())
        return nums;

    std::stable_sort(nums.begin(), nums.end());

    for (auto&& it = nums.begin();it != nums.end() - 1;)
    {
        if (*it != *(it + 1))
            it = nums.erase(it);
        else
            ++it;
    }

    nums.erase(nums.end() - 1);

    return nums;
}

std::vector<bool> LeetCode::prefixesDivBy5(std::vector<int>& A)
{
    std::vector<bool> result;

    //auto&& strA = StringUtility::CombineString(A, "");

    int last{};
    for (int i = 0;i <= (int)A.size() - 1;++i)
    {
        /*std::string str(strA.begin(), strA.begin() + i);
        cout<<str.c_str()<<" ";*/

        auto&& current = (last << 1) + A[i];
        current %= 10;
        cout<< current <<endl;

        result.emplace_back(current % 5 == 0);

        last = current;
    }

    return result;
}

int LeetCode::findLHS(std::vector<int>& nums)
{
    if (nums.empty())
        return 0;

    std::map<int, int> mapNums;

    for (const auto& n : nums)
        ++mapNums[n];

    int result{};

    auto&& it_last = mapNums.begin();
    for (auto it = ++mapNums.begin();it != mapNums.end();++it)
    {
        if (it->first - it_last->first == 1)
            result = std::max(it->second + it_last->second, result);
        it_last = it;
    }

    return result;
}

std::vector<int> LeetCode::findDisappearedNumbers(std::vector<int>& nums)
{
    std::set<int> setNums(nums.begin(), nums.end());

    nums.clear();
    for (int i = 1;i <= (int)nums.capacity();++i)
    {
        if (setNums.find(i) == setNums.end())
            nums.emplace_back(i);
    }
    return nums;
}

bool LeetCode::canThreePartsEqualSum(std::vector<int>& A)
{
    if (A.size() < 3)
        return false;

    int sum = accumulate(A.begin(), A.end(), 0);
    if (sum % 3 != 0)
        return false;

    int avg = sum / 3;

    int end = canEqualSum(avg, A, 0);
    if (end == -1)
        return false;

    end = canEqualSum(avg, A, end);
    if (end == -1)
        return false;

    end = canEqualSum(avg, A, end);
    if (end == -1)
        return false;

    return true;
}

int LeetCode::largestSumAfterKNegations(std::vector<int>& A, int K)
{
    for (int i = 0;i < K;++i)
    {
        auto&& it = std::min_element(A.begin(), A.end());
        *it = -*it;
    }
    return accumulate(A.begin(), A.end(), 0);
}

std::vector<int> LeetCode::addToArrayForm(std::vector<int>& A, int K)
{
    auto&& vecK = intToArray(K);

    std::reverse(A.begin(), A.end());
    std::reverse(vecK.begin(), vecK.end());

    std::vector<int> res;

    int carry{};
    for (int i{};i < (int)A.size() || i < (int)vecK.size();++i)
    {
        int value1 = i < (int)A.size() ? A[i] : 0;
        int value2 = i < (int)vecK.size() ? vecK[i] : 0;
        int sum = value1 + value2 + carry;
        carry = sum > 9 ? 1 : 0;
        sum %= 10;
        res.emplace_back(sum);
    }

    if (carry == 1)
    {
        res.emplace_back(1);
    }

    std::reverse(res.begin(), res.end());
    return res;
}

std::string LeetCode::addBinary(std::string a, std::string b)
{
    std::reverse(a.begin(), a.end());
    std::reverse(b.begin(), b.end());

    std::string result{};

    int carry{};
    for (int i{};i < (int)a.size() || i < (int)b.size();++i)
    {
        int value1 = i < (int)a.size() ? a[i] - '0' : 0;
        int value2 = i < (int)b.size() ? b[i] - '0' : 0;
        int sum = value1 + value2 + carry;
        carry = sum > 1 ? 1 : 0;
        sum = sum > 1 ? sum - 2 : sum;
        result += to_string(sum);
    }

    if (carry == 1)
    {
        result += to_string(carry);
    }

    std::reverse(result.begin(), result.end());
    return result;
}

int LeetCode::singleNonDuplicate(std::vector<int>& nums)
{
    //std::stable_sort(nums.begin(), nums.end());
    for (int i = 0;i < (int)nums.size() - 1;i += 2)
    {
        if (nums[i] != nums[i + 1])
            return nums[i];
    }
    return nums[nums.size() - 1];
}

int LeetCode::countPrimes(int n)
{
    std::vector<int> result;
    for (int i = 2;i < n;++i)
    {
        result.emplace_back(i);
    }

    for (auto&& it_sieve = result.begin();it_sieve != result.end();++it_sieve)
    {
        for (auto&& it = it_sieve + 1;it != result.end();)
        {
            if (*it % *it_sieve == 0)
                it = result.erase(it);
            else
                ++it;
        }
    }

    return result.size();
}

vector<int> LeetCode::findErrorNums(vector<int>& nums)
{
    vector<int> result;

    auto&& nSize = nums.size();
    for (int i{ 1 };i <= (int)nSize;++i)
    {
        auto&& it = std::find(nums.begin(), nums.end(), i);
        if (it == nums.end())
            result.emplace_back(i);
        else
            nums.erase(it);
    }
    result.insert(result.begin(), nums.begin(), nums.end());

    return result;
}

bool LeetCode::buddyStrings(string A, string B)
{
    if (A.size() < 2 || B.size() < 2)
        return false;

    if (A.length() != B.length())
        return false;

    //

    string subA, subB;
    for (int i = 0;i < (int)A.size();++i)
    {
        if (A[i] != B[i])
        {
            subA.push_back(A[i]);
            subB.push_back(B[i]);
        }
    }

    if (subA.size() == 2)
    {
        Algorithm::Swap(subA[0], subA[1]);
        return subA == subB;
    }

    if (A == B)
    {
        stable_sort(A.begin(), A.end());
        auto&& it = std::unique(A.begin(), A.end());
        return it != A.end();
    }

    return false;
}

int LeetCode::trailingZeroes(int n)
{
    if (n < 5) return 0;
    int k = n / 5;
    return k + trailingZeroes(k);
}

int LeetCode::findNthDigit(int n)
{
    int i{ 0 };
    while (n > 0)
    {
        n -= caculateDigits(++i);
    }
    auto&& str = to_string(i);
    return str[str.length() - 1 + n] - '0';
}

int LeetCode::numPairsDivisibleBy60(std::vector<int>& time)
{
    int count{};
    for (int i = 0; i < (int)time.size() - 1; i++)
    {
        for_each(time.begin() + i + 1, time.end(), [&](int t) 
        {
            if ((time[i] + t) % 60 == 0)
                ++count;
        });
    }
    return count;
}

std::vector<std::vector<int>> LeetCode::kClosest(std::vector<std::vector<int>>& points, int K)
{
    std::vector<std::vector<int>> result;

    if (K <= 0 || K > (int)points.size())
    {
		return result;
    }

	nth_element(points.begin(), points.begin() + K - 1, points.end(), [](const std::vector<int>& point1, const std::vector<int>& point2)
	{
		return distanceSquare(point1) < distanceSquare(point2);
	});

	result.insert(result.begin(), points.begin(), points.begin() + K);

    return result;
}

int LeetCode::twoCitySchedCost(vector<vector<int>>& costs)
{
	std::sort(costs.begin(), costs.end(), [](const vector<int>& cost1, const vector<int>& cost2)
	{
		return abs(cost1[0] - cost1[1]) > abs(cost2[0] - cost2[1]);
	});

	int result{};

	std::vector<int> vecCount{0, 0};
	for (const auto& cost : costs)
	{
		if (vecCount[0] == costs.size() / 2)
		{
			result += cost[1];
			++vecCount[1];
			continue;
		}

		if (vecCount[1] == costs.size() / 2)
		{
			result += cost[0];
			++vecCount[0];
			continue;
		}

		if (cost[0] < cost[1])
		{
			result += cost[0];
			++vecCount[0];
		}
		else
		{
			result += cost[1];
			++vecCount[1];
		}
	}

	return result;
}

int LeetCode::heightChecker(vector<int>& heights)
{
	int result{};
	auto tempVec = heights;
	sort(tempVec.begin(), tempVec.end());
	for (int i = 0;i < (int)tempVec.size();++i)
	{
		if (tempVec[i] != heights[i])
		{
			++result;
		}
	}
	return result;
}

static int findPairs1(vector<int>& nums, int &k)
{
	std::map<int, int> tempMap;
	for (auto n : nums)
	{
		++tempMap[n];
	}

	int count{ 0 };
	for (auto p : tempMap)
	{
		p.second > 1 ? ++count : NULL;
	}
	return count;
}

static int findPairs2(vector<int>& nums, int &k)
{
	std::sort(nums.begin(), nums.end());
	nums.erase(std::unique(nums.begin(), nums.end()), nums.end());

	int count{ 0 };
	for (size_t i = 0;i < nums.size();++i)
	{
		for (size_t j = i + 1; j < nums.size(); ++j)
		{
			if (nums[j] - nums[i] == k)
			{
				++count;
				cout << nums[j] << " " << nums[i] << endl;
				break;
			}
			else if (nums[j] - nums[i] > k)
			{
				break;
			}
		}
	}
	return count;
}

int LeetCode::findPairs(vector<int>& nums, int k)
{
	if (k < 0)
	{
		return 0;
	}
	else if (k == 0)
	{
		return findPairs1(nums, k);
	}
	else
	{
		return findPairs2(nums, k);
	}
}

int LeetCode::findShortestSubArray(vector<int>& nums)
{
	auto&& size = nums.size();
	if (size < 2)
	{
		return size;
	}

	struct data
	{
		int degree{};
		vector<int> indexVec{};
		int length{-1};
	};
	std::map<int, data> resultMap;

	for (int i = 0; i < (int)size; ++i)
	{
		++resultMap[nums[i]].degree;
		resultMap[nums[i]].indexVec.emplace_back(i);
	}

	int result{INT_MAX};
	int degree{};
	for (auto&& p : resultMap)
	{
		if (p.second.degree < degree)
		{
			continue;
		}

		if (p.second.length == -1)
		{
			p.second.length = p.second.indexVec[p.second.indexVec.size() - 1] - p.second.indexVec[0] + 1;
		}
		
		if ((p.second.degree > degree) || (p.second.degree == degree && p.second.length < result))
		{
			result = p.second.length;
		}

		degree = p.second.degree;
	}

	return result;
}

int LeetCode::pivotIndex(vector<int>& nums)
{
	//sumLeft + sumRight + nums[p] = sumTotal; 
	//sumLeft = sumRight 
	//可以得出 sumLeft * 2 + nums[p] = sumTotal;

	int sumLeft{0};
	int sumTotal = accumulate(nums.begin(), nums.end(), 0);

	for (int i = -1; i < (int)nums.size() - 1; ++i)
	{
		if (i >= 0)
		{
			sumLeft += nums[i];
		}

		if (sumLeft * 2 + nums[i + 1] == sumTotal)
		{
			return i + 1;
		}
	}

	return -1;
}

int LeetCode::repeatedStringMatch(string A, string B)
{
	return -1;
}

static bool validPalindrome(string s, int start, int end)
{
    while (start < end)
    {
        if (s[start] != s[end])
            return false;
        ++start;
        --end;
    }
    return true;
}

bool LeetCode::validPalindrome(string s)
{
    if (s.length() < 2)
    {
        return true;
    }

    int start = 0;
    int end = s.length() - 1;
    while (start < end)
    {
        if (s[start] != s[end])
        {
            return ::validPalindrome(s, start+1, end) || ::validPalindrome(s, start, end-1);
        }
        else
        {
            ++start;
            --end;
        }
    }

    return true;
}

int LeetCode::minMoves2(vector<int>& nums)
{
    if (nums.size() < 2)
    {
        return 0;
    }

    sort(nums.begin(), nums.end());

    int result{ 0 };
    int start{ 1 };
    int end{ 1 };
    while (start + end <= (int)nums.size())
    {
        if (nums[start] == nums[start - 1])
        {
            ++start;
        }
        
        if (nums[nums.size() - end] == nums[nums.size() - end - 1])
        {
            ++end;
        }

        if (start <= end)
        {
            result += start * (nums[start] - nums[start - 1]);
            ++start;
        }
        else
        {
            result += end * (nums[nums.size() - end] - nums[nums.size() - end - 1]);
            ++end;
        }
    }

    return result;
}

string LeetCode::removeOuterParentheses(string S)
{
    string result{};
    
    int ref{1};
    for (int i = 1;i < (int)S.length();++i)
    {
        S[i] == '(' ? ++ref : --ref;
        if (ref == 0)
        {
            ++i;
            ref = 1;
            continue;
        }
        result.push_back(S[i]);
    }

    return result;
}

int LeetCode::calPoints(vector<string>& ops)
{
    std::stack<int> s;
    for (const std::string& str : ops)
    {
        if (str == "+")
        {
            int value2 = s.top();
            s.pop();
            int value1 = s.top();
            s.pop();
            int sum = value1 + value2;
            s.push(value1);
            s.push(value2);
            s.push(sum);
        }
        else if (str == "D")
        {
            int value = s.top();
            s.push(value * 2);
        }
        else if (str == "C")
        {
            s.pop();
        }
        else
        {
            s.push(std::stoi(str));
        }
    }

    int result{};
    while (!s.empty())
    {
        int value = s.top();
        s.pop();
        result += value;
    }
    return result;
}

vector<int> LeetCode::relativeSortArray(vector<int>& arr1, vector<int>& arr2)
{
    std::map<int, int> mapArr2;
    for (int i = 0; i < (int)arr2.size(); ++i)
    {
        mapArr2[arr2[i]] = i + 1;
    }

    std::stable_sort(arr1.begin(), arr1.end(), [&mapArr2](const int& v1, const int& v2)
    {
        int weight1 = mapArr2[v1] == 0 ? INT_MAX : mapArr2[v1];
        int weight2 = mapArr2[v2] == 0 ? INT_MAX : mapArr2[v2];
        if (weight1 != weight2)
        {
            return weight1 < weight2;
        }
        else
        {
            return v1 < v2;
        }
    });

    return arr1;
}

static vector<int> findRook(vector<vector<char>>& board)
{
    vector<int> result{};
    for (int i = 0; i < (int)board.size(); ++i)
    {
        for (int j = 0; j < (int)board[i].size(); ++j)
        {
            if (board[i][j] == 'R')
            {
                result.emplace_back(i);
                result.emplace_back(j);
                return result;
            }
        }
    }
    return result;
}

static bool isTopRookCaptures(vector<vector<char>>& board, int i, int j)
{
    for (int Index = i; Index >= 0; --Index)
    {
        if (board[Index][j] == 'B')
        {
            return false;
        }
        if (board[Index][j] == 'p')
        {
            return true;
        }
    }
    return false;
}

static bool isBottomRookCaptures(vector<vector<char>>& board, int i, int j)
{
    for (int Index = i; Index < (int)board.size(); ++Index)
    {
        if (board[Index][j] == 'B')
        {
            return false;
        }
        if (board[Index][j] == 'p')
        {
            return true;
        }
    }
    return false;
}

static bool isLeftRookCaptures(vector<vector<char>>& board, int i, int j)
{
    for (int Index = j; Index >= 0; --Index)
    {
        if (board[i][Index] == 'B')
        {
            return false;
        }
        if (board[i][Index] == 'p')
        {
            return true;
        }
    }
    return false;
}

static bool isRightRookCaptures(vector<vector<char>>& board, int i, int j)
{
    for (int Index = i; Index < (int)board[i].size(); ++Index)
    {
        if (board[i][Index] == 'B')
        {
            return false;
        }
        if (board[i][Index] == 'p')
        {
            return true;
        }
    }
    return false;
}

int LeetCode::numRookCaptures(vector<vector<char>>& board)
{
    auto indexes = findRook(board);

    int result{};
    if (isTopRookCaptures(board, indexes[0], indexes[1]))
    {
        ++result;
    }
    if (isBottomRookCaptures(board, indexes[0], indexes[1]))
    {
        ++result;
    }
    if (isLeftRookCaptures(board, indexes[0], indexes[1]))
    {
        ++result;
    }
    if (isRightRookCaptures(board, indexes[0], indexes[1]))
    {
        ++result;
    }

    return result;
}

vector<string> LeetCode::findOcurrences(string text, string first, string second)
{
	StringVec words{};
	StringUtility::SplitString(words, text.c_str(), " ");

	vector<string> result{};
	for (int i = 0; i < (int)words.size() - 2; ++i)
	{
		if (words[i] == first && words[i + 1] == second)
			result.emplace_back(words[i + 2]);
	}
	return result;
}

enum DirectionType
{
	North = 0,
	West = 1,
	South = 2,
	East = 3,
};

static int getDirection(int direction, int command)
{
	switch (command)
	{
	case -2:
	{
		++direction;
		direction = direction > 3 ? 0 : direction;
	}
		break;
	case -1:
	{
		--direction;
		direction = direction < 0 ? 3 : direction;
	}
		break;
	default:
		break;
	}

	LOGD("getDirection:%d", direction);

	return direction;
}

static bool isCollision(const vector<int>& position, const vector<vector<int>>& obstacles)
{
	for (const auto& obstacle : obstacles)
	{
		if (position[0] == obstacle[0] && position[1] == obstacle[1])
		{
			LOGD("isCollision:%d %d", obstacle[0], obstacle[1]);
			return true;
		}
	}
	return false;
}

static vector<int> doMove(const vector<int>& position, int direction, int step, const vector<vector<int>>& obstacles)
{
	vector<int> result = position;

	for (int i = 0; i < step; ++i)
	{
		auto last = result;
		switch (direction) 
		{
		case North:
			++result[1];
			break;
		case West:
			--result[0];
			break;
		case South:
			--result[1];
			break;
		case East:
			++result[0];
			break;
		}

		if (isCollision(result, obstacles))
		{
			result = last;
		}

		LOGD("doMove:%d %d", result[0], result[1]);
	}

	return result;
}

int LeetCode::robotSim(vector<int>& commands, vector<vector<int>>& obstacles)
{
	vector<int> position{0, 0};
	int direction = North;

	for (auto command : commands)
	{
		if (command < 0)
		{
			direction = getDirection(direction, command);
		}
		else
		{
			position = doMove(position, direction, command, obstacles);
		}
	}

	return pow(position[0], 2) + pow(position[1], 2);
}

bool LeetCode::validMountainArray(vector<int>& A)
{
	if (A.size() < 3)
	{
		return false;
	}

	bool rise{ true };
	for (int i = 0; i < (int)A.size() - 1 ; ++i)
	{
		if (A[i] == A[i + 1])
			return false;
		else if (A[i] > A[i + 1])
		{
			if (i == 0)
			{
				return false;
			}
			rise = false;
		}
		else
		{
			if (!rise)
			{
				return false;
			}
		}
	}

	return !rise;
}

vector<vector<int>> LeetCode::largeGroupPositions(string S)
{
	vector<vector<int>> result{};

	if (S.length() < 3)
	{
		return result;
	}

	std::stack<char> s{};
	int start{};

	for (int i = 0; i < (int)S.length(); ++i)
	{
		if (s.empty() || S[i] == s.top())
		{
			s.push(S[i]);
		}
		else
		{
			if (s.size() >= 3)
			{
				result.emplace_back(vector<int>{ start, int(start + s.size() - 1) });
			}
			printStack(s);
			clearStack<char>(s);

			start = i;
			s.push(S[i]);
		}
	}

	if (s.size() >= 3)
	{
		result.emplace_back(vector<int>{ start, int(start + s.size() - 1) });
	}
	printStack(s);

	return result;
}

int LeetCode::dayOfYear(std::string date)
{
	StringVec result;
	StringUtility::SplitString(result, date.c_str(), "-");

	bool leapYear = isLeapYear(StringUtility::StringToInt(result[0]));

	int day = StringUtility::StringToInt(result[2]);
	for (int i = 1;i < StringUtility::StringToInt(result[1]);++i)
	{
		day += getDayOfMonth(i, leapYear);
	}

	return day;
}

bool LeetCode::isLeapYear(int year)
{
	return (year % 4 == 0) && (year % 100 != 0);
}

int LeetCode::getDayOfMonth(int month, bool leapYear)
{
	if (month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month == 10 || month == 12)
	{
		return 31;
	}

	if (month == 2)
	{
		return leapYear ? 29 : 28;
	}

	return 30;
}

std::string LeetCode::say(std::string str)
{
	std::string result;
	std::stack<char> s;
	for (int i = 0;i < (int)str.length();++i)
	{
		if (s.empty() || str[i] == s.top())
		{
			s.push(str[i]);
		}
		else
		{
			result += StringUtility::IntToString(s.size()) + std::string(1, s.top());
			clearStack<char>(s);
			s.push(str[i]);
		}
	}

	if (!s.empty())
	{
		result += StringUtility::IntToString(s.size()) + std::string(1, s.top());
	}

	return result;
}

string LeetCode::countAndSay(int n)
{
	if (n < 1)
	{
		return "";
	}

	if (n == 1)
	{
		return "1";
	}

	return say(countAndSay(n - 1));
}

bool LeetCode::isLetters(const char& c)
{
    return (c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z');
}

int LeetCode::maxProfit(std::vector<int>& prices, int start, int end)
{
    if (end - start < 1)
        return 0;

    auto&& mid = start + (end - start) / 2;

    return std::max(maxProfitCross(prices, start, end, mid),
        std::max(maxProfit(prices, start, mid), maxProfit(prices, mid + 1, end)));
}

int LeetCode::maxProfitCross(std::vector<int>& prices, int start, int end, int mid)
{
    auto&& max_element_right = *std::max_element(prices.begin() + mid + 1, prices.begin() + end + 1);
    auto&& min_element_left = *std::min_element(prices.begin() + start, prices.begin() + mid + 1);
    return max_element_right - min_element_left;
}

int LeetCode::canEqualSum(int sum, std::vector<int>& A, int start)
{
    for (int i = start;i < (int)A.size();++i)
    {
        sum -= A[i];
        if (sum == 0)
            return i + 1;
    }

    return -1;
}

std::vector<int> LeetCode::intToArray(int K)
{
    std::vector<int> res;
    auto&& strK = to_string(K);
    for (const auto& c : strK)
    {
        res.emplace_back(c - '0');
    }
    return res;
}

int LeetCode::caculateDigits(int n)
{
    return 1 + (int)log10(n);
}

int LeetCode::distanceSquare(const std::vector<int>& point)
{
    if (point.size() < 2)
        return 0;
    return point[0] * point[0] + point[1] * point[1];
}

std::string LeetCode::printStack(std::stack<char>& s)
{
	while (!s.empty())
	{
		std::cout << s.top();
		s.pop();
	}
	std::cout << endl;
	return "";
}
