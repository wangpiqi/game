#ifndef _ALGORITHM_H_
#define _ALGORITHM_H_

#include <vector>
using namespace std;

class Algorithm
{
public:
    //插入排序
    static vector<int> InsertionSort(const vector<int>& input);
    //归并排序
    static vector<int> MergeSort(const vector<int>& left, const vector<int>& right);
    //二分查找
    static int BinarySearch(int n, int num);
    //最大子序和
    static vector<int> FindMaxCrossingSubArray(const vector<int>& input, int low, int mid, int high);
    static vector<int> FindMaxSubArray(const vector<int>& input, int low, int high);
    //交换
    template <class T>
    static void Swap(T& a, T& b)
    {
        auto temp = a;
        a = b;
        b = temp;
    }
};

#endif