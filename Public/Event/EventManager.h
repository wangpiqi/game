#pragma once

#include "../Utility/Singleton.h"
#include <functional>
#include <map>
#include <unordered_map>
#include "EventDefine.h"

class Event;

#ifndef REGISTER_EVENT
#define REGISTER_EVENT(eventId, func)\
EventManager::GetSingleton().registerEvent(eventId, Delegate{ this, std::bind(&func, this, std::placeholders::_1) });
#endif

#ifndef UNREGISTER_EVENT
#define UNREGISTER_EVENT(eventId, func)\
EventManager::GetSingleton().unregisterEvent(eventId, Delegate{ this, std::bind(&func, this, std::placeholders::_1) });
#endif

using CallBack = std::function<bool(const Event&)>;

struct Delegate
{
	void* m_ptr{};
	CallBack m_callback{};

	bool operator==(const Delegate& delegate)
	{
		return m_ptr == delegate.m_ptr;
	}
};

class EventManager : public Singleton<EventManager>
{
public:
	bool registerEvent(EVENT_ID eventId, const Delegate& delegate, int priority = 0);
	bool unregisterEvent(EVENT_ID eventId, const Delegate& delegate);
	bool postEvent(const Event& event);

private:
	using DelegateMap = std::multimap<int, Delegate>;
	using DelegateMapMap = std::unordered_map<EVENT_ID, DelegateMap>;
	DelegateMapMap m_DelegateMapMap;
};