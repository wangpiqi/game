#pragma once

enum class EVENT_ID
{
	EVENT_ID_TEST,
	EVENT_ID_PROPERTY_CHANGED,
	EVENT_ID_MAX,
};