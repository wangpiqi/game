#include "EventManager.h"
#include "Event.h"

bool EventManager::registerEvent(EVENT_ID eventId, const Delegate& delegate, int priority/* = 0*/)
{
	m_DelegateMapMap[eventId].emplace(std::make_pair(priority, delegate));
	return true;
}

bool EventManager::unregisterEvent(EVENT_ID eventId, const Delegate& delegate)
{
	auto it = m_DelegateMapMap.find(eventId);
	if (it == m_DelegateMapMap.end())
		return false;

	for (auto it_pair = it->second.begin();it_pair != it->second.end();++it_pair)
	{
		if (it_pair->second == delegate)
		{
			it->second.erase(it_pair);
			break;
		}
	}

	return false;
}

bool EventManager::postEvent(const Event& event)
{
	auto&& it = m_DelegateMapMap.find(event.m_EventId);
	if (it == m_DelegateMapMap.end())
		return false;

	for (auto&& p : it->second)
	{
		if (p.second.m_callback(event))
		{
			break;
		}
	}

	return false;
}
