#pragma once

#ifndef _STRINGUTILITY_H_
#define _STRINGUTILITY_H_

#include <string>
#include <vector>

typedef std::vector<std::string> StringVec;
typedef std::vector<int> IntVec;

class StringUtility
{
public:
    static bool StringIsNull(const char* str);
    static bool IsPalindrome(const std::string& str);
    static bool SplitString(StringVec& result, const char* str, const char* delim = ",");
    static std::string CombineString(const StringVec& input, const char* delim = ",");
    static std::string CombineString(const IntVec& input, const char* delim = ",");
    template <class T>
    static std::string CombineString(const std::vector<T>& input, const char* delim = ",")
    {
        if (input.size() == 0)
            return "";

        std::string result = "";
        for (const auto& e : input)
        {
            if (std::is_same<T, std::string>::value)
            {
                result += (e + delim);
            }
            else
            {
                result += (to_string(e) + delim);
            }
        }

        result = result.substr(0, result.length() - strlen(delim));

        return result;
    }
    static std::string IntToString(int input);
	static std::vector<char> IntToChars(int input);
    static std::string FloatToString(float input);
    static int StringToInt(const std::string& str);
	static std::wstring StringToWStr(const std::string& str);
	static int compress(std::vector<char>& chars);
	static bool StringMatchTail(const std::string& str, const std::string& tail);
	static bool StringMatchHead(const std::string& str, const std::string& head);
};

#endif