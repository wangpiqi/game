#include "PlayerPrefs.h"

AnyMap PlayerPrefs::m_AnyMap;

#ifndef GENERATE_GET_FUNCTION
#define GENERATE_GET_FUNCTION(NAME, TYPE)\
TYPE PlayerPrefs::Get##NAME(const std::string& key, TYPE defaultValue)\
{\
	auto it = m_AnyMap.find(key);\
	if (it != m_AnyMap.end() && it->second.type() == typeid(TYPE))\
	{\
		return std::any_cast<TYPE>(it->second);\
	}\
	return defaultValue;\
}
#endif

#ifndef GENERATE_SET_FUNCTION
#define GENERATE_SET_FUNCTION(NAME, TYPE)\
void PlayerPrefs::Set##NAME(const std::string& key, const TYPE& value)\
{\
	auto it = m_AnyMap.find(key);\
	if (it != m_AnyMap.end())\
	{\
		if (it->second.type() != typeid(TYPE))\
			return;\
		m_AnyMap.erase(it);\
	}\
	m_AnyMap.emplace(std::make_pair(key, value));\
}
#endif

void PlayerPrefs::DeleteAll()	//Removes all keys and values from the preferences. Use with caution.
{
    m_AnyMap.clear();
}

void PlayerPrefs::DeleteKey(const std::string& key)	//Removes key and its corresponding value from the preferences.
{
    auto it = m_AnyMap.find(key);
    if (it != m_AnyMap.end())
    {
        m_AnyMap.erase(it);
    }
}

GENERATE_GET_FUNCTION(Float, float);	//Returns the value corresponding to key in the preference file if it exists.
GENERATE_GET_FUNCTION(Int, int);	//Returns the value corresponding to key in the preference file if it exists.
GENERATE_GET_FUNCTION(String, std::string);	//Returns the value corresponding to key in the preference file if it exists.

bool PlayerPrefs::HasKey(const std::string& key)	//Returns true if key exists in the preferences.
{
    auto it = m_AnyMap.find(key);
    return it != m_AnyMap.end();
}

void PlayerPrefs::Save()	//Writes all modified preferences to disk.
{
    //TODO
}

GENERATE_SET_FUNCTION(Float, float);	//Sets the value of the preference identified by key.
GENERATE_SET_FUNCTION(Int, int)	//Sets the value of the preference identified by key.
GENERATE_SET_FUNCTION(String, std::string);	//Sets the value of the preference identified by key.
