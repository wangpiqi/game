#pragma once

#include <any>

#ifndef _MACROUTILITY_H_
#define _MACROUTILITY_H_

#ifndef MY_NEW
#define MY_NEW new
#endif

#ifndef MY_DELETE
#define MY_DELETE delete
#endif

#ifndef IN
#define IN
#endif

#ifndef OUT
#define OUT
#endif

#ifndef INOUT
#define INOUT
#endif

#ifndef SAFE_DELETE
#define SAFE_DELETE(p)\
if (p)\
{\
    MY_DELETE (p);\
    (p) = nullptr;\
}
#endif

//#ifndef DELETE
//#define DELETE(p)\
//if (p)\
//{\
//    delete (p);\
//}
//#endif

#ifndef NULL_RETURN
#define NULL_RETURN(p)\
if ((p) == nullptr)\
{\
    return;\
}
#endif

#ifndef NULL_RETURN_VALUE
#define NULL_RETURN_VALUE(p, v)\
if ((p) == nullptr)\
{\
    return (v);\
}
#endif

#ifndef DLL_EXPORT
#ifdef _WIN32
#define DLL_EXPORT __declspec(dllexport)
#else
#define DLL_EXPORT
#endif
#endif

using Any = std::any;

#ifndef DECLARE_PROPERTY_MAP
#define DECLARE_PROPERTY_MAP	\
public:\
	bool HasProperty(const std::string& key)\
	{\
		return m_mapProperty.find(key) != m_mapProperty.end(); \
	}\
\
	template <class T>\
	auto GetProperty(const std::string& key)\
	{\
		auto it = m_mapProperty.find(key);\
		if (it == m_mapProperty.end())\
		{\
			assert(0);\
			throw;\
		}\
		return std::any_cast<T>(it->second);\
	}\
\
	template <class T>\
	void SetProperty(const std::string& key, const T& value)\
	{\
		auto it = m_mapProperty.find(key);\
		if (it != m_mapProperty.end())\
		{\
			if (it->second.type() != typeid(T))\
				return;\
			if (*std::any_cast<T>(&(it->second)) == value)\
				return;\
			m_mapProperty.erase(it);\
		}\
		m_mapProperty.emplace(std::make_pair(key, value));\
		EventManager::GetSingleton().postEvent(Event{ EVENT_ID::EVENT_ID_PROPERTY_CHANGED, \
		VarList() << this << key << value });\
	}\
\
protected:\
	std::unordered_map<std::string, Any> m_mapProperty;
#endif

#endif