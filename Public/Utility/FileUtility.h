#pragma once

#include <string>

class FileUtility
{
public:
	static std::string ReadFile(const char* path);
	static std::string GetResourcePath();
};