#include "FileUtility.h"
#include <fstream>
#include <assert.h>
#include <vector>

std::string FileUtility::ReadFile(const char * path)
{
	std::string strPath = GetResourcePath() + std::string(path);
	std::ifstream stream(strPath.c_str(), std::ios::in | std::ios::binary | std::ios::ate);
	if (!stream.is_open())
	{
		assert(0);
		return "";
	}

	auto size = stream.tellg();
	std::string str(size, '\0'); // 构造 string 为流大小
	stream.seekg(0);
	stream.read(&str[0], size);

	stream.close();

	return str;
}

std::string FileUtility::GetResourcePath()
{
	return "Resource/";
}
