#pragma once

#include "MacroUtility.h"
#include <string>
#include <unordered_map>
#include <any>

using AnyMap = std::unordered_map<std::string, Any>;

class PlayerPrefs
{
public:
    static void DeleteAll();	//Removes all keys and values from the preferences. Use with caution.
    static void DeleteKey(const std::string& key);	//Removes key and its corresponding value from the preferences.
    static float GetFloat(const std::string& key, float defaultValue = 0.0f);	//Returns the value corresponding to key in the preference file if it exists.
    static int GetInt(const std::string& key, int defaultValue = 0);	//Returns the value corresponding to key in the preference file if it exists.
    static std::string GetString(const std::string& key, std::string defaultValue = "");	//Returns the value corresponding to key in the preference file if it exists.
    static bool HasKey(const std::string& key);	//Returns true if key exists in the preferences.
    static void Save();	//Writes all modified preferences to disk.
    static void SetFloat(const std::string& key, const float& value);	//Sets the value of the preference identified by key.
    static void SetInt(const std::string& key, const int& value);	//Sets the value of the preference identified by key.
    static void SetString(const std::string& key, const std::string& value);	//Sets the value of the preference identified by key.

private:
    static AnyMap m_AnyMap;
};