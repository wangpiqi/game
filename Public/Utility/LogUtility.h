#pragma once

#ifdef _WIN32
#else
#include <android/log.h>
#endif

enum class LogPriority
{
    Info = 4,
    Debug = 3,
    Warning = 5,
    Error = 6,
};

#define LOGD(format, ...) LogUtility::print(LogPriority::Debug, format, ##__VA_ARGS__);
#define LOGI(format, ...) LogUtility::print(LogPriority::Info, format, ##__VA_ARGS__);
#define LOGW(format, ...) LogUtility::print(LogPriority::Warning, format, ##__VA_ARGS__);
#define LOGE(format, ...) LogUtility::print(LogPriority::Error, format, ##__VA_ARGS__);

class LogUtility
{
public:
    static void print(LogPriority prio, const char* format, ...);
};