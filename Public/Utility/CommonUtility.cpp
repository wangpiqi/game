#include "CommonUtility.h"

uint64_t CommonUtility::genUniqueId()
{
	static uint64_t nUniqueId{ 0 };
	return nUniqueId++;
}
