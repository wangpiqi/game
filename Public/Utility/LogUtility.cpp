#include "LogUtility.h"
#include <stdarg.h>
#include <stdio.h>
#include <iostream>
#ifdef _WIN32
#include <Windows.h>
#include <xlocbuf>
#endif
#include <vector>

void LogUtility::print(LogPriority prio, const char* format, ...)
{
    va_list args;

    va_start(args, format);

#ifdef _WIN32
	int nLength = _vscprintf(format, args) + 1; //上面返回的长度是包含\0，这里加上
	std::vector<char> vectorChars(nLength);
	_vsnprintf(vectorChars.data(), nLength, format, args);
	std::string str(vectorChars.data());
	std::cout << str.c_str() << std::endl;

    std::wstring_convert<std::codecvt<wchar_t, char, std::mbstate_t>>
    converter(new std::codecvt<wchar_t, char, std::mbstate_t>("CHS"));
    //std::string narrow_str = converter.to_bytes(wide_str);
    std::wstring wstr = converter.from_bytes(str.c_str()) + L"\n";
    OutputDebugString(wstr.c_str());
#else
    __android_log_vprint((int)prio  , "", format, args);
#endif

    va_end(args);
}