#pragma once

#ifndef _SINGLETON_H_
#define _SINGLETON_H_

template <class T>
class Singleton
{
protected:
    virtual ~Singleton() {};

public:
    static T& GetSingleton()
    {
        return m_instance;
    };

private:
    static T m_instance;
};

template <class T>
T Singleton<T>::m_instance;

#endif