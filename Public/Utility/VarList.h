#pragma once

#include <type_traits>
#include <vector>
#include "MacroUtility.h"
#include <assert.h>

class VarList
{
public:
	VarList& operator<<(const Any& data)
	{
		m_list.emplace_back(data);
		return *this;
	}

    template <typename T>
    T get(int index) const
    {
        if (index < 0 || index >= (int)m_list.size())
        {
			assert(0);
        }
        return std::any_cast<T>(m_list[index]);
    };

private:
    std::vector<Any> m_list;
};