#include "StringUtility.h"
#include <sstream>
#include <algorithm>
#include <iostream>

using namespace std;

bool StringUtility::StringIsNull(const char* str)
{
    return (str == nullptr || *str == '\0');
}

bool StringUtility::IsPalindrome(const std::string& str)
{
    std::string temp = str;
    reverse(temp.begin(), temp.end());
    return temp == str;
}

bool StringUtility::SplitString(StringVec& result, const char* str, const char* delim/* = ","*/)
{
    if (StringIsNull(str) || StringIsNull(delim))
        return false;

    std::string temp(str);

    int pos = 0;
    while ((pos = temp.find(delim)) != std::string::npos)
    {
        result.emplace_back(temp.substr(0, pos));
        temp = temp.substr(pos + strlen(delim));
    }

    result.emplace_back(temp);

    return true;
}

std::string StringUtility::CombineString(const StringVec& input, const char* delim/* = ","*/)
{
    if (input.size() == 0)
        return "";

    std::string result = "";
    for (const auto& str : input)
    {
        result += (str + delim);
    }

    result = result.substr(0, result.length() - strlen(delim));

    return result;
}

std::string StringUtility::CombineString(const IntVec& input, const char* delim/* = ","*/)
{
    if (input.size() == 0)
        return "";

    std::string result = "";
    for (const auto& n : input)
    {
        result += (IntToString(n) + delim);
    }

    result = result.substr(0, result.length() - strlen(delim));

    return result;
}

std::string StringUtility::IntToString(int input)
{
    std::stringstream s;
    s << input;
    return s.str();
}

std::vector<char> StringUtility::IntToChars(int input)
{
	std::vector<char> result;

	std::string s = StringUtility::IntToString(input);
	for (int i = 0; i < (int)s.size(); ++i)
	{
		result.emplace_back(s[i]);
	}

	return result;
}

std::string StringUtility::FloatToString(float input)
{
    std::stringstream s;
    s << input;
    return s.str();
}

int StringUtility::StringToInt(const std::string& str)
{
    return atoi(str.c_str());
}

std::wstring StringUtility::StringToWStr(const std::string & str)
{
	std::wstring result(str.begin(), str.end());
	return result;
}

int StringUtility::compress(vector<char>& chars)
{
	vector<char> result;

	char last = '\0';

	int count = 0;
	for (auto c : chars)
	{
		if (last != '\0' && last != c)
		{
			if (count != 1)
			{
				auto&& chars = StringUtility::IntToChars(count);
				result.insert(result.end(), chars.begin(), chars.end());
			}
			count = 0;
		}

		if (count == 0)
		{
			result.emplace_back(c);
		}

		++count;

		last = c;
	}

	if (count != 1)
	{
		auto&& chars = StringUtility::IntToChars(count);
		result.insert(result.end(), chars.begin(), chars.end());
	}

	result.swap(chars);

	std::cout<< std::string(chars.begin(), chars.end()).c_str() << std::endl;

	return chars.size();
}

bool StringUtility::StringMatchTail(const std::string & str, const std::string & tail)
{
	if (tail.length() > str.length())
	{
		return false;
	}

	return str.substr(str.length() - tail.length()) == tail;
}

bool StringUtility::StringMatchHead(const std::string & str, const std::string & head)
{
	if (head.length() > str.length())
	{
		return false;
	}

	return str.substr(0, head.length()) == head;
}
