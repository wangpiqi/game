#pragma once

#include "IComponent.h"
#include "MacroUtility.h"
#include "../Event/EventManager.h"
#include <any>
#include <string>
#include <unordered_map>

class Component : public IComponent
{
public:
    virtual ~Component() = default;

    virtual bool Init(Object* pObject) override { return true; };
    virtual bool Run(int timeInterval) override { return true; };
    virtual bool Shut() override { return true; };

	DECLARE_PROPERTY_MAP;

protected:
	//
};