#pragma once

class Object;

class IComponent
{
public:
    virtual ~IComponent() = default;

    virtual bool Init(Object* pObject) = 0;
    virtual bool Run(int timeInterval) = 0;
    virtual bool Shut() = 0;
};